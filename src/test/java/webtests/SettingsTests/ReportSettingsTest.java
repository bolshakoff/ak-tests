package webtests.SettingsTests;

import org.junit.Test;
import pages.MainHeader;
import pages.SettingsPages.MainSettingsPage;
import pages.SettingsPages.ReportSettingsPage;
import webtests.FunctionalTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static webtests.LoginTest.correctLogin;

public class ReportSettingsTest extends FunctionalTest {

    @Test
    public void checkAllFields() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        ReportSettingsPage reportSettingsPage = mainSettingsPage.reportsLinkClick();

        assertTrue(reportSettingsPage.isWeeklyReportCheckboxExist());
        assertTrue(reportSettingsPage.isVulnerabilityDetectionCheckboxExist());
        assertTrue(reportSettingsPage.isAttacksSelectionCheckboxExist());
        assertTrue(reportSettingsPage.isLoginReportCheckboxExist());
        assertTrue(reportSettingsPage.isUnauthorizedCheckboxExist());
        assertTrue(reportSettingsPage.isSaveButtonExist());
    }

    @Test
    public void allCheckboxEnable() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        ReportSettingsPage reportSettingsPage = mainSettingsPage.reportsLinkClick();

        reportSettingsPage.loginReportCheckboxClick();
        reportSettingsPage.unautorizedCheckboxClick();

        reportSettingsPage.attacksSetectionCheckboxClick();
        assertTrue(reportSettingsPage.isAttacksIntervalInputExist());
        reportSettingsPage.setAttacksIntervalInput("1000");

        reportSettingsPage.saveButtonClick();
        assertEquals("×\n" +
                "Сохранено успешно", reportSettingsPage.getSuccessDivText());
    }

//    //TODO Сделать проверку на то, включены ли чекбоксы, если включены, то выключить
//    @After
//    public void tearDown() {
//        ReportSettingsPage reportSettingsPage = new ReportSettingsPage(driver);
//        reportSettingsPage.loginReportCheckboxClick();
//        reportSettingsPage.unautorizedCheckboxClick();
//        reportSettingsPage.attacksSetectionCheckboxClick();
//    }
}
