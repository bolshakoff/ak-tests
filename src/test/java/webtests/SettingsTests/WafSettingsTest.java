package webtests.SettingsTests;

import org.junit.Test;
import pages.MainHeader;
import pages.SettingsPages.MainSettingsPage;
import pages.SettingsPages.WafSettingsPage;
import webtests.FunctionalTest;

import static org.junit.Assert.assertTrue;
import static webtests.LoginTest.correctLogin;

public class WafSettingsTest extends FunctionalTest {
    @Test
    public void checkAllFields() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        WafSettingsPage wafSettingsPage = mainSettingsPage.wafLinkClick();

//        assertTrue(wafSettingsPage.activeScannerSelectExist());

        assertTrue(wafSettingsPage.isCacheInputExist());
        assertTrue(wafSettingsPage.isAttackCountExist());
        assertTrue(wafSettingsPage.isBanIpDurationExist());
    }
}
