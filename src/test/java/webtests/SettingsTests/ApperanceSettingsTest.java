package webtests.SettingsTests;

import org.junit.Test;
import pages.MainHeader;
import pages.SettingsPages.ApperanceSettingsPage;
import pages.SettingsPages.MainSettingsPage;
import webtests.FunctionalTest;

import static org.junit.Assert.assertTrue;
import static webtests.LoginTest.correctLogin;

public class ApperanceSettingsTest extends FunctionalTest {

    @Test
    public void checkAllFields() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        ApperanceSettingsPage apperanceSettingsPage = mainSettingsPage.apperanceLinkClick();

        assertTrue(apperanceSettingsPage.isLanguageSelectExist());
        assertTrue(apperanceSettingsPage.isColorSelectExist());

        apperanceSettingsPage.selectLanguage(1);
        apperanceSettingsPage.selectColor(1);
        apperanceSettingsPage.saveButtonClick();

        apperanceSettingsPage.selectLanguage(0);
        apperanceSettingsPage.selectColor(0);
        apperanceSettingsPage.saveButtonClick();
    }
}
