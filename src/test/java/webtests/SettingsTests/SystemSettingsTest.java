package webtests.SettingsTests;

import org.junit.Test;
import pages.MainHeader;
import pages.SettingsPages.MainSettingsPage;
import pages.SettingsPages.SystemSettingsPage;
import webtests.FunctionalTest;

import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;
import static org.awaitility.Awaitility.await;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static webtests.LoginTest.correctLogin;

public class SystemSettingsTest extends FunctionalTest {
    @Test
    public void allFieldsExist() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        SystemSettingsPage systemSettingsPage = mainSettingsPage.systemLinkClick();


        assertTrue(systemSettingsPage.isPostSizeInputExist());
        assertTrue(systemSettingsPage.isLogStorageTimeExist());
        assertTrue(systemSettingsPage.isLicenseTextareaExist());
        assertTrue(systemSettingsPage.isLicenseTypeTextExist());
        assertTrue(systemSettingsPage.isCheckIntegrityExist());
        assertEquals("Тип лицензии: Полноценная\n" +
                "Максимальное число доменов: Без ограничений\n" +
                "Лицензия на ядро: бессрочно\n" +
                "Лицензия на WAF: бессрочно\n" +
                "Лицензия на AntiDDoS: бессрочно\n" +
                "Лицензия на CCS: бессрочно", systemSettingsPage.getLicenseType());
    }

    @Test
    public void checkIncorrectValues() throws InterruptedException {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        SystemSettingsPage systemSettingsPage = mainSettingsPage.systemLinkClick();

        //incorrect postSize

        for (int i = -1; i < 2; i++) {
            systemSettingsPage.typePostSize(Integer.toString(i));
            systemSettingsPage.saveButtonClick();
            sleep(200);
            assertEquals("Значение «Допустимый размер тела запроса (MB)» должно быть не меньше 2.", systemSettingsPage.getPostSizeErrorText());
        }

        for (char ch : "xyz".toCharArray()) {
            systemSettingsPage.typePostSize(Character.toString(ch));
            systemSettingsPage.saveButtonClick();
            sleep(200);
            assertEquals("Значение «Допустимый размер тела запроса (MB)» должно быть целым числом.", systemSettingsPage.getPostSizeErrorText());
        }

        systemSettingsPage.typePostSize("9999999999999");
        systemSettingsPage.saveButtonClick();
        sleep(200);
        assertEquals("Значение «Допустимый размер тела запроса (MB)» не должно превышать 999999999999.", systemSettingsPage.getPostSizeErrorText());

        systemSettingsPage.typePostSize("");
        systemSettingsPage.saveButtonClick();
        sleep(200);
        assertEquals("Необходимо заполнить «Допустимый размер тела запроса (MB)».", systemSettingsPage.getPostSizeErrorText());

//        systemSettingsPage.typePostSize("30");

        //incorrect LogStorageTime
        for (int i = -1; i < 1; i++) {
            systemSettingsPage.typeLogFilterTimeInput(Integer.toString(i));
            systemSettingsPage.saveButtonClick();
            sleep(200);
            assertEquals("Значение «Период фильтрации логов (дней)» должно быть не меньше 1.", systemSettingsPage.getLogFilterTimeErrorText());
        }

        for (char ch : "xyz".toCharArray()) {
            systemSettingsPage.typeLogFilterTimeInput(Character.toString(ch));
            systemSettingsPage.saveButtonClick();
            sleep(200);
            assertEquals("Значение «Период фильтрации логов (дней)» должно быть целым числом.", systemSettingsPage.getLogFilterTimeErrorText());
        }

        systemSettingsPage.typeLogFilterTimeInput("9923372036854775808");
        systemSettingsPage.saveButtonClick();
        sleep(200);
        assertEquals("Значение «Период фильтрации логов (дней)» не должно превышать 9223372036854775807.", systemSettingsPage.getLogFilterTimeErrorText());

        systemSettingsPage.typeLogFilterTimeInput("");
        systemSettingsPage.saveButtonClick();
        sleep(200);
        assertEquals("Необходимо заполнить «Период фильтрации логов (дней)».", systemSettingsPage.getLogFilterTimeErrorText());

        //incorrect LogStorageTime

        for (int i = -1; i < 1; i++) {
            systemSettingsPage.typeLogStorageTimeInput(Integer.toString(i));
            systemSettingsPage.saveButtonClick();
            sleep(200);
            assertEquals("Значение «Максимальный период хранения логов доступа (месяцев)» должно быть не меньше 1.", systemSettingsPage.getLogStorageTimeErrorText());
        }

        for (char ch : "xyz".toCharArray()) {
            systemSettingsPage.typeLogStorageTimeInput(Character.toString(ch));
            systemSettingsPage.saveButtonClick();
            sleep(200);
            assertEquals("Значение «Максимальный период хранения логов доступа (месяцев)» должно быть целым числом.", systemSettingsPage.getLogStorageTimeErrorText());
        }

        systemSettingsPage.typeLogStorageTimeInput("92233720368547758071");
        systemSettingsPage.saveButtonClick();
        sleep(200);
        assertEquals("Значение «Максимальный период хранения логов доступа (месяцев)» не должно превышать 9223372036854775807.", systemSettingsPage.getLogStorageTimeErrorText());

        systemSettingsPage.typeLogStorageTimeInput("");
        systemSettingsPage.saveButtonClick();
        sleep(200);
        assertEquals("Необходимо заполнить «Максимальный период хранения логов доступа (месяцев)».", systemSettingsPage.getLogStorageTimeErrorText());
    }

    @Test
    public void checkCorrectValues() throws InterruptedException {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        SystemSettingsPage systemSettingsPage = mainSettingsPage.systemLinkClick();

        systemSettingsPage.typePostSize("30");
        sleep(200);
        assertEquals(0, systemSettingsPage.getPostSizeErrorText().length());

        systemSettingsPage.typeLogStorageTimeInput("10");
        sleep(200);
        assertEquals(0, systemSettingsPage.getPostSizeErrorText().length());

        systemSettingsPage.saveButtonClick();
        assertEquals("×\n" +
                "Сохранено успешно", systemSettingsPage.getSuccessDivText());
    }

    @Test
    public void checkIntegrityStatus() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        SystemSettingsPage systemSettingsPage = mainSettingsPage.systemLinkClick();

        systemSettingsPage.checkIntegriryClick();
        await().atMost(10, TimeUnit.SECONDS).with().pollInterval(1, TimeUnit.SECONDS)
                .untilAsserted(() -> assertEquals("Целостность приложения не нарушена", systemSettingsPage.getIntegrityText()));
    }
}