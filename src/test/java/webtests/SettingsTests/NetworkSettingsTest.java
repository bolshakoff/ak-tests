package webtests.SettingsTests;

import org.junit.Test;
import pages.MainHeader;
import pages.SettingsPages.MainSettingsPage;
import pages.SettingsPages.NetworkSettingPage;
import webtests.FunctionalTest;

import static org.junit.Assert.assertTrue;
import static webtests.LoginTest.correctLogin;

public class NetworkSettingsTest extends FunctionalTest {
    @Test
    public void checkAllFields() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        NetworkSettingPage networkSettingPage = mainSettingsPage.networkLinkClick();

        assertTrue(networkSettingPage.isNatCheckboxExist());
        assertTrue(networkSettingPage.isTcpPortsInputExist());
        assertTrue(networkSettingPage.isUdpPortsInputExist());
        assertTrue(networkSettingPage.isIpv6CheckboxExist());
        assertTrue(networkSettingPage.isWebCheckboxExist());
        assertTrue(networkSettingPage.isSaveButtonExist());
    }
}
