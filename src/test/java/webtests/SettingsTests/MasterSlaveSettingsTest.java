package webtests.SettingsTests;

import org.junit.Test;
import pages.MainHeader;
import pages.SettingsPages.MainSettingsPage;
import pages.SettingsPages.MasterSlaveSettingsPage;
import webtests.FunctionalTest;

import static org.junit.Assert.assertTrue;
import static webtests.LoginTest.correctLogin;

public class MasterSlaveSettingsTest extends FunctionalTest {
    @Test
    public void checkAllFieldsExist() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        MasterSlaveSettingsPage masterSlaveSettingsPage = mainSettingsPage.masterSlaveLinkClick();

        assertTrue(masterSlaveSettingsPage.isMasterIpInputExist());
        assertTrue(masterSlaveSettingsPage.isConnectButtonExist());
        assertTrue(masterSlaveSettingsPage.isApiKeyInputExist());
        assertTrue(masterSlaveSettingsPage.isApiSyncPortInputExist());
        assertTrue(masterSlaveSettingsPage.isSaveButtonExist());
    }
}