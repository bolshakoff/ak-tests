package webtests.SettingsTests;

import org.junit.Test;
import pages.MainHeader;
import pages.SettingsPages.MainSettingsPage;
import pages.SettingsPages.SyslogSettingsPage;
import webtests.FunctionalTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static webtests.LoginTest.correctLogin;

public class SyslogSettingsTest extends FunctionalTest {

    @Test
    public void allFieldsExist() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        SyslogSettingsPage syslogSettingsPage = mainSettingsPage.syslogLinkClick();

        assertTrue(syslogSettingsPage.isSyslogEnableCheckboxExist());
        assertTrue(syslogSettingsPage.isSyslogIpInputExist());
        assertTrue(syslogSettingsPage.isSaveButtonExist());
    }

    @Test
    public void CheckSyslogEnable() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        SyslogSettingsPage syslogSettingsPage = mainSettingsPage.syslogLinkClick();

        syslogSettingsPage.syslogEnableCheckboxClick();
        syslogSettingsPage.setSyslogIpInput(pcIp);
        syslogSettingsPage.saveButtonClick();
        assertEquals("×\n" +
                "Сохранено успешно", syslogSettingsPage.getSuccessDivText());
    }
}
