package webtests.SettingsTests;

import org.junit.Test;
import pages.MainHeader;
import pages.SettingsPages.MainSettingsPage;
import pages.SettingsPages.ZabbixSettingsPage;
import webtests.FunctionalTest;

import static org.junit.Assert.assertTrue;
import static webtests.LoginTest.correctLogin;

public class ZabbixSettingsTest extends FunctionalTest {
    @Test
    public void checkAllFields() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        ZabbixSettingsPage zabbixSettingsPage = mainSettingsPage.zabbixLinkClick();

        assertTrue(zabbixSettingsPage.isIpInputExist());
        assertTrue(zabbixSettingsPage.isPortinputExist());
        assertTrue(zabbixSettingsPage.isSaveButtonExist());
    }
}
