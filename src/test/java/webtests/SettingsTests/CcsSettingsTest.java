package webtests.SettingsTests;

import org.junit.Test;
import pages.MainHeader;
import pages.SettingsPages.CcsSettingsPage;
import pages.SettingsPages.MainSettingsPage;
import webtests.FunctionalTest;

import static java.lang.Thread.sleep;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static webtests.LoginTest.correctLogin;

public class CcsSettingsTest extends FunctionalTest {
    @Test
    public void checkAllFieldsExist() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        CcsSettingsPage ccsSettingsPage = mainSettingsPage.ccsLinkClick();
        assertTrue(ccsSettingsPage.isMemoryInputExist());
        assertTrue(ccsSettingsPage.isRuntimeTimeoutInputExist());
        assertTrue(ccsSettingsPage.isSaveButtonExist());
    }

    @Test
    public void checkIncorrectCcsMemory() throws InterruptedException {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        final CcsSettingsPage ccsSettingsPage = mainSettingsPage.ccsLinkClick();

        for (int i = -1; i < 4; i++) {
            ccsSettingsPage.setMemoryInput(Integer.toString(i));
            ccsSettingsPage.saveButtonClick();
            sleep(200);
            assertEquals("Значение «Количество выделяемой памяти для сканирований CCS (GB)» должно быть не меньше 4.", ccsSettingsPage.getMemoryErrorText());
        }
    }

    @Test
    public void checkCorrectCcsMemory() throws InterruptedException {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        final CcsSettingsPage ccsSettingsPage = mainSettingsPage.ccsLinkClick();

        for (int i = 5; i < 9; i++) {
            ccsSettingsPage.setMemoryInput(Integer.toString(i));
            sleep(300);
            assertEquals(0, ccsSettingsPage.getMemoryErrorText().length());
        }
    }

    // TODO: Добавить проверку кнопки "Изменить статусы уязвимостей"
}