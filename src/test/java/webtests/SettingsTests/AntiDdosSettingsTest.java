package webtests.SettingsTests;

import org.junit.Test;
import pages.MainHeader;
import pages.SettingsPages.AntiDdosSettingsPage;
import pages.SettingsPages.MainSettingsPage;
import webtests.FunctionalTest;

import static org.junit.Assert.assertTrue;
import static webtests.LoginTest.correctLogin;

public class AntiDdosSettingsTest extends FunctionalTest {
    @Test
    public void checkAllFields() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        AntiDdosSettingsPage antiDdosSettingsPage = mainSettingsPage.antiddosLinkClick();

        assertTrue(antiDdosSettingsPage.isRatioInputExist());
        assertTrue(antiDdosSettingsPage.isDurationInputExist());
        assertTrue(antiDdosSettingsPage.isMinTrafficInputExist());
        assertTrue(antiDdosSettingsPage.isFromAntiDdosCheckboxExist());
    }
}
