package webtests.SettingsTests;

import org.junit.Test;
import pages.MainHeader;
import pages.SettingsPages.MainSettingsPage;
import pages.SettingsPages.SecuritySettingsPage;
import webtests.FunctionalTest;

import static org.junit.Assert.assertTrue;
import static webtests.LoginTest.correctLogin;

public class SecuritySettingsTest extends FunctionalTest {
    @Test
    public void checkAllFields() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        SecuritySettingsPage securitySettingsPage = mainSettingsPage.securityLinkClick();

        assertTrue(securitySettingsPage.isMinPassInputExist());
        assertTrue(securitySettingsPage.isUpperLowerCheckboxExist());
        assertTrue(securitySettingsPage.isNnumbersCheckboxExist());
        assertTrue(securitySettingsPage.isSpecialCharCheckboxExist());
        assertTrue(securitySettingsPage.isIntervalInputExist());
        assertTrue(securitySettingsPage.isAttemptsInputExist());
        assertTrue(securitySettingsPage.isDurationInputExist());
        assertTrue(securitySettingsPage.isSertificateFileInputExist());
        assertTrue(securitySettingsPage.isKeyFileInputExist());
    }
}
