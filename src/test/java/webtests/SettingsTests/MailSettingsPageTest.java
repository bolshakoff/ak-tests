package webtests.SettingsTests;

import org.junit.Test;
import pages.MainHeader;
import pages.SettingsPages.MailSettingsPage;
import pages.SettingsPages.MainSettingsPage;
import webtests.FunctionalTest;

import static org.junit.Assert.assertEquals;
import static webtests.LoginTest.correctLogin;

public class MailSettingsPageTest extends FunctionalTest {
    @Test
    public void CheckCorrect() throws InterruptedException {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        MailSettingsPage mailSettingsPage = mainSettingsPage.mailLinkClick();
        mailSettingsPage.typeHostEmail("alxbol@yandex.ru");
        mailSettingsPage.typeHost("smtp.yandex.ru");
        mailSettingsPage.typeUsername("alxbol");
        mailSettingsPage.typePassword("Becmo4ka");
        mailSettingsPage.typePort("465");
        mailSettingsPage.selectEncryption(1);
        mailSettingsPage.saveButtonClick();
        assertEquals("×\n" +
                "Сохранено успешно", mailSettingsPage.getSuccessDivText());
        mailSettingsPage.checkButtonClick();
        mailSettingsPage.checkButton2Click();
        System.out.println(mailSettingsPage.getCheckStatusText());
//        System.out.println(mailSettingsPage.getAlertText());
    }
}
