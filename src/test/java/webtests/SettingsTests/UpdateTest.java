package webtests.SettingsTests;

import org.junit.Test;
import pages.MainHeader;
import pages.SettingsPages.MainSettingsPage;
import pages.SettingsPages.UpdateSettingsPage;
import webtests.FunctionalTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static webtests.LoginTest.correctLogin;

public class UpdateTest extends FunctionalTest {
    @Test
    public void checkAllFieldsExist() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        UpdateSettingsPage updateSettingsPage = mainSettingsPage.updateLinkClick();

        assertTrue(updateSettingsPage.isAutoUpdateCheckboxExist());
        assertTrue(updateSettingsPage.isCurrentVersionTextExist());
        assertTrue(updateSettingsPage.isSaveButtonExist());
    }

    @Test
    public void checkEnableDisableAutoUpdate() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        MainSettingsPage mainSettingsPage = mainHeader.settingsLinkClick();
        UpdateSettingsPage updateSettingsPage = mainSettingsPage.updateLinkClick();

        updateSettingsPage.autoUpdateCheckboxClick();
        updateSettingsPage.saveButtonClick();
        assertEquals("×\n" +
                "Сохранено успешно", updateSettingsPage.getSuccessDivText());
    }
}
