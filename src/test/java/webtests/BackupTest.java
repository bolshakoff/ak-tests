package webtests;

import org.junit.Test;
import pages.BackupPage;
import pages.MainHeader;

import static webtests.LoginTest.correctLogin;


public class BackupTest extends FunctionalTest {
    @Test
    public void Backup() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        BackupPage backupPage = mainHeader.backupLinkClick();
        backupPage.createBackupButtonClick();
    }
}
