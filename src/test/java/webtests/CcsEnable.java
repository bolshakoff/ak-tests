package webtests;

import org.junit.Ignore;
import org.junit.Test;
import pages.CcsPage;
import pages.DomainPages.AttacksPage;
import pages.DomainPages.DomainSettingPage;
import pages.SitesPage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static webtests.LoginTest.correctLogin;

public class CcsEnable extends FunctionalTest {
    @Test
    public void checkCcsEnable() throws InterruptedException {
        correctLogin();
        SitesPage sitesPage = new SitesPage(driver);
        DomainSettingPage domainSettingPage = sitesPage.createSiteButtonClick();
        domainSettingPage.typeDomainName(siteDomain);
        domainSettingPage.selectServerIp(0);
        domainSettingPage.ccsEnableCheckboxClick();
        assertEquals("Включено", domainSettingPage.getCcsEnableInfo());
        AttacksPage attacksPage = domainSettingPage.saveButtonClick();
        attacksPage.waitUntilSave();
        attacksPage.editLinkClick();
        domainSettingPage.removeButtonClick();
        domainSettingPage.confirmButtonClick();
        domainSettingPage.waitUntilRemove();
        assertFalse(sitesPage.isWafGraphicsExist());
    }

    @Ignore
    public void checkCcsZip() throws InterruptedException {
        correctLogin();
        SitesPage sitesPage = new SitesPage(driver);
        DomainSettingPage domainSettingPage = sitesPage.createSiteButtonClick();
        domainSettingPage.typeDomainName(siteDomain);
        domainSettingPage.selectServerIp(0);
        domainSettingPage.ccsEnableCheckboxClick();
        assertEquals("Включено", domainSettingPage.getCcsEnableInfo());
        AttacksPage attacksPage = domainSettingPage.saveButtonClick();
        attacksPage.waitUntilSave();
        CcsPage ccsPage = attacksPage.ccsLinkClick();
        ccsPage.addModuleButtonClick();

    }
}
