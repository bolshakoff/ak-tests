package webtests;

import junit.framework.JUnit4TestAdapter;
import junit.framework.TestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.AllTests;
import webtests.SettingsTests.AntiDdosSettingsTest;
import webtests.SettingsTests.ApperanceSettingsTest;
import webtests.UsersTests.*;

@RunWith(AllTests.class)
public class AllTestSuite {

    public static TestSuite suite() {
        TestSuite suite = new TestSuite();
        suite.addTest(new JUnit4TestAdapter(AddAdminTest.class));
        suite.addTest(new JUnit4TestAdapter(BackupTest.class));
        suite.addTest(new JUnit4TestAdapter(LoginTest.class));
        suite.addTest(new JUnit4TestAdapter(CreateUserTest.class));
        suite.addTest(new JUnit4TestAdapter(CreateRoleTest.class));
        suite.addTest(new JUnit4TestAdapter(RolesUserTest.class));
        suite.addTest(new JUnit4TestAdapter(EditUserTest.class));
        suite.addTest(new JUnit4TestAdapter(UserTest.class));
        suite.addTest(new JUnit4TestAdapter(CcsEnable.class));
        suite.addTest(new JUnit4TestAdapter(AntiDdosSettingsTest.class));
        suite.addTest(new JUnit4TestAdapter(ApperanceSettingsTest.class));

        return suite;
    }
}