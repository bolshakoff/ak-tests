package webtests;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.MainHeader;

import java.io.*;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class FunctionalTest {
    public static WebDriver driver;
    protected static String adminLogin;
    private String domainName;
    private String webConnType;
    protected static String adminPassword;
    protected static String siteIp;
    protected String pcIp;
    protected static String siteDomain;
    protected String adminEmail;
    private String webIp;
    private String sshUser;
    private String sshPasswd;
    private Session sshSession;

    private void getProperties() {
        FileInputStream fis;
        Properties property = new Properties();

        try {
            fis = new FileInputStream("src/main/resources/config.properties");
            property.load(fis);
            siteIp = property.getProperty("site_ip");
            pcIp = property.getProperty("pc_ip");
            domainName = property.getProperty("domain");
            siteDomain = property.getProperty("site_domain");
            adminLogin = property.getProperty("admin_login");
            adminPassword = property.getProperty("admin_password");
            adminEmail = property.getProperty("admin_email");
            webConnType = property.getProperty("web_connection_type");
            webIp = property.getProperty("web_ip");
            sshUser = property.getProperty("ssh_user");
            sshPasswd = property.getProperty("ssh_password");
        } catch (IOException e) {
            System.err.println("ОШИБКА: Файл свойств отсуствует!");
        }
    }

    private Session getSshSession(String host, String username, String passwd) throws JSchException {
        JSch conn = new JSch();
        Session session = conn.getSession(username, host, 22);
        session.setPassword(passwd);
        session.setConfig("StrictHostKeyChecking", "no");
        session.connect();
        return session;
    }

    private void getRuntimeLogFile(Session session) throws JSchException {
        ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
        channel.connect();
        try {
            InputStream in = channel.get("/var/www/node/docs/runtime/logs.log");
            String lf = "logs/" + this.getClass().getSimpleName() + ".log";
            FileOutputStream targetFile = new FileOutputStream(lf);

            int c;
            while ((c = in.read()) != -1) {
                targetFile.write(c);
            }

            in.close();
            targetFile.close();
        } catch (Exception ignored) {
        }
        channel.disconnect();
    }

    private void cleanRuntimeLog(Session session) throws JSchException {
        ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
        channel.connect();
        try {
            channel.rm("/var/www/node/docs/runtime/logs/error.log");
        } catch (Exception ignored) {
        }
        channel.disconnect();
    }

    @Before
    public void setUp() throws JSchException, IOException {
        // Clean temp directory
        FileUtils.cleanDirectory(new File("tmp"));

        getProperties();
        String osName = System.getProperty("os.name").toLowerCase();
        if ( osName.contains("win") ) {
            System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver/windows/chromedriver.exe");
        }
        if ( osName.contains("linux") ) {
            System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver/linux/chromedriver");
        }
//        driver = new HighlightingWrapper(new ChromeDriver(), 300);
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.get(webConnType + "://" + domainName);

        sshSession = getSshSession(webIp, sshUser, sshPasswd);
        cleanRuntimeLog(sshSession);
    }

    @After
    public void tearDown() throws JSchException, InterruptedException {
        new MainHeader(driver).logoutPageClick();
        driver.quit();
        getRuntimeLogFile(sshSession);
        sshSession.disconnect();
    }
}

