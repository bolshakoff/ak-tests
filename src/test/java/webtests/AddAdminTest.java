package webtests;

import org.junit.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.AddAdminPage;


public class AddAdminTest extends FunctionalTest {
    @Test
    public void correctRegistration() {
        AddAdminPage addAdminPage = new AddAdminPage(driver);
        addAdminPage.typeAdminName(adminLogin);
        addAdminPage.typeEmail(adminEmail);
        addAdminPage.typePassword(adminPassword);
        addAdminPage.typePasswordConfirm(adminPassword);
        addAdminPage.submitButtonClick();

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleIs("Авторизоваться"));
    }

//    @Test
//    public void incorrectRegistration() {
//    }
}
