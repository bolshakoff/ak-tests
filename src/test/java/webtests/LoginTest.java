package webtests;

import pages.LoginPage;
import pages.SitesPage;


public class LoginTest extends FunctionalTest {
    public static SitesPage correctLogin() {
        LoginPage loginPage = new LoginPage(driver);
        return loginPage.correctLogin(adminLogin, adminPassword);

    }
}
