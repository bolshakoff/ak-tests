package webtests.UsersTests;

import org.junit.After;
import org.junit.Test;
import pages.CreateUserPage;
import pages.EditUserPage;
import pages.MainHeader;
import pages.UserPage;
import webtests.FunctionalTest;

import static org.junit.Assert.assertEquals;
import static webtests.LoginTest.correctLogin;

public class CreateUserTest extends FunctionalTest {
    private void createUser(String name, String email, String password, int roleIndex) {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        UserPage userPage = mainHeader.userLinkClick();
        userPage.createLinkClick();
        CreateUserPage createUserPage = userPage.createUserLinkClick();

        createUserPage.setUserName(name);
        createUserPage.setEmail(email);
        createUserPage.setPassword(password);
        createUserPage.selectRole(roleIndex);
        EditUserPage editUserPage = createUserPage.saveButtonClick();
        assertEquals("Пользователь был создан", editUserPage.getAlertText());
    }

    @Test
    public void createAdmin() {
        createUser("admin2", "admin2@mail.ru", "P@$$w0rd", 0);
    }

    @Test
    public void createUser() {
        createUser("user", "user@mail.ru", "P@$$w0rd", 1);
    }

    @After
    public void tearDown() {
        EditUserPage editUserPage = new EditUserPage(driver);
        UserPage userPage = editUserPage.deleteClick();
        assertEquals("Пользователь был удален", userPage.getWarningText());
    }
}
