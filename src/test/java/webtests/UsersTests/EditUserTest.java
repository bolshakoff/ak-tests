package webtests.UsersTests;

import org.junit.After;
import org.junit.Test;
import pages.CreateUserPage;
import pages.EditUserPage;
import pages.MainHeader;
import pages.UserPage;
import webtests.FunctionalTest;

import static java.lang.Thread.sleep;
import static org.junit.Assert.*;
import static webtests.LoginTest.correctLogin;

public class EditUserTest extends FunctionalTest {
    private EditUserPage createUser(String name, String email, String password, int roleIndex) {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        UserPage userPage = mainHeader.userLinkClick();
        userPage.createLinkClick();
        CreateUserPage createUserPage = userPage.createUserLinkClick();

        createUserPage.setUserName(name);
        createUserPage.setEmail(email);
        createUserPage.setPassword(password);
        createUserPage.selectRole(roleIndex);
        EditUserPage editUserPage = createUserPage.saveButtonClick();
        assertEquals("Пользователь был создан", editUserPage.getAlertText());
        return editUserPage;
    }

    @Test
    public void changeAdminToManager() throws InterruptedException {
        EditUserPage editUserPage = createUser("admin2", "admin2@mail.ru", "P@$$w0rd", 0);
        assertTrue(editUserPage.isPassDisabled());
        editUserPage.changePassCheckboxClick();
        assertFalse(editUserPage.isPassDisabled());
        editUserPage.setName("admin3");
        editUserPage.setEmail("admin3@mail.ru");
        editUserPage.setPassword("P@$$w0rd3");
        editUserPage.selectRole(1);
        editUserPage.selectRole(0);
        editUserPage.updateButtonClick();
        assertEquals("Пользователь был создан", editUserPage.getAlertText());
        sleep(200);
        UserPage userPage = editUserPage.userLinkClick();
        assertTrue(userPage.getAllNames().containsKey("admin3"));
        assertTrue(userPage.getAllEmails().containsKey("admin3@mail.ru"));
        assertTrue(userPage.getAllRoles().containsKey("manager (Менеджер)"));
    }

    @After
    public void tearDown() {
        UserPage userPage = new UserPage(driver);
        userPage.deleteClick(1);
        assertEquals("Пользователь был удален", userPage.getWarningText());
    }
}
