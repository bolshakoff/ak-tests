package webtests.UsersTests;

import org.junit.Test;
import pages.MainHeader;
import pages.RoleUserPage;
import pages.UserPage;
import webtests.FunctionalTest;

import static org.junit.Assert.*;
import static webtests.LoginTest.correctLogin;

public class RolesUserTest extends FunctionalTest {
    @Test
    public void checkAdminManagerRoles() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        UserPage userPage = mainHeader.userLinkClick();
        RoleUserPage roleUserPage = userPage.rolesLinkClick();

        // Check admin role
        assertEquals("admin", roleUserPage.getName(0));
        assertEquals("Админ", roleUserPage.getDescription(0));
        assertFalse(roleUserPage.isButtonExist(0));

        // Check manager role
        assertEquals("manager", roleUserPage.getName(1));
        assertEquals("Менеджер", roleUserPage.getDescription(1));
        assertTrue(roleUserPage.isButtonExist(1));
    }
}
