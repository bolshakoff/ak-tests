package webtests.UsersTests;

import org.junit.After;
import org.junit.Test;
import pages.CreateRolePage;
import pages.MainHeader;
import pages.RoleUserPage;
import pages.UserPage;
import webtests.FunctionalTest;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static webtests.LoginTest.correctLogin;

public class CreateRoleTest extends FunctionalTest {
    private final String ROLE_NAME = "adminRole";
    private final String MANAGER_ROLE_NAME = "managerRole";

    @Test
    public void checkRulesCount() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        UserPage userPage = mainHeader.userLinkClick();
        userPage.createLinkClick();
        CreateRolePage createRolePage = userPage.createRoleLinkClick();
        assertEquals(179, createRolePage.getRulesListElements().size());
    }

    @Test
    public void createAdminRole() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        UserPage userPage = mainHeader.userLinkClick();
        userPage.createLinkClick();
        CreateRolePage createRolePage = userPage.createRoleLinkClick();
        createRolePage.setRoleName(ROLE_NAME);
        String ROLE_DESCRIPTION = "test adminRole";
        createRolePage.setDescription(ROLE_DESCRIPTION);
        createRolePage.setRule(0);
        RoleUserPage roleUserPage = createRolePage.saveButtonClick();
        assertTrue(roleUserPage.getAllNames().containsKey(ROLE_NAME));
        assertTrue(roleUserPage.getAllDescription().containsKey(ROLE_DESCRIPTION));
    }

    @Test
    public void createManagerRole() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        UserPage userPage = mainHeader.userLinkClick();
        userPage.createLinkClick();
        CreateRolePage createRolePage = userPage.createRoleLinkClick();
        createRolePage.setRoleName(MANAGER_ROLE_NAME);
        String MANAGER_ROLE_DESCRIPTION = "test managerRole";
        createRolePage.setDescription(MANAGER_ROLE_DESCRIPTION);
        createRolePage.setRule(1);
        RoleUserPage roleUserPage = createRolePage.saveButtonClick();
        assertTrue(roleUserPage.getAllNames().containsKey(MANAGER_ROLE_NAME));
        assertTrue(roleUserPage.getAllDescription().containsKey(MANAGER_ROLE_DESCRIPTION));
    }

    @After
    public void tearDown() {
        RoleUserPage roleUserPage = new RoleUserPage(driver);
        if ( roleUserPage.getTitle().contains("Роли") ) {
            Map<String, Integer> roleNamesMap = roleUserPage.getAllNames();
            if ( roleNamesMap.containsKey(ROLE_NAME) ) {
                roleUserPage.deleteRole(roleNamesMap.get(ROLE_NAME));
            }
            if ( roleNamesMap.containsKey(MANAGER_ROLE_NAME) ) {
                roleUserPage.deleteRole(roleNamesMap.get(MANAGER_ROLE_NAME));
            }
        }
    }
}
