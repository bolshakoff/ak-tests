package webtests.UsersTests;

import org.junit.Test;
import pages.MainHeader;
import pages.UserPage;
import webtests.FunctionalTest;

import static org.junit.Assert.assertEquals;
import static webtests.LoginTest.correctLogin;

public class UserTest extends FunctionalTest {
    @Test
    public void adminExist() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        UserPage userPage = mainHeader.userLinkClick();

        assertEquals(adminLogin, userPage.getName(1));
        assertEquals(adminEmail, userPage.getEmail(1));
        assertEquals("admin (Админ)", userPage.getRole(1));
    }

    @Test
    public void removeAndBlockYourself() {
        correctLogin();
        MainHeader mainHeader = new MainHeader(driver);
        UserPage userPage = mainHeader.userLinkClick();

        userPage.deleteClick(1);
        assertEquals("Вы не можете удалить свой собственный аккаунт", userPage.getWarningText());

        userPage.blockClick(1);
        assertEquals("Вы не можете заблокировать свою учетную запись", userPage.getWarningText());
    }
}
