package webtests.DomainTests;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.common.IOUtils;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.schmizz.sshj.transport.verification.PromiscuousVerifier;
import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;
import pages.DomainPages.AttacksPage;
import pages.DomainPages.DomainSettingPage;
import pages.LoginPage;
import pages.SitesPage;
import webtests.FunctionalTest;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class AddDomainTest extends FunctionalTest {
    static void addHttpsWafSettings() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        SitesPage sitesPage = loginPage.correctLogin(adminLogin, adminPassword);
        assertFalse(sitesPage.isWafGraphicsExist());
        DomainSettingPage domainSettingPage = sitesPage.createSiteButtonClick();
        domainSettingPage.typeDomainName(siteDomain);
        domainSettingPage.selectServerIp(0);
        domainSettingPage.enabledCheckbox();
        domainSettingPage.typeSiteIpInput(siteIp);
        domainSettingPage.selectWafMode(2);//Блокировка
        domainSettingPage.enableHttps();
        domainSettingPage.addSslCert(new File("src/main/resources/dwvaloc.crt"));
        domainSettingPage.addSslKey(new File("src/main/resources/dwvaloc.key"));
        AttacksPage attacksPage = domainSettingPage.saveButtonClick();
        attacksPage.waitUntilSave();
    }

    static void removeDomain() throws InterruptedException {
        AttacksPage attacksPage = new AttacksPage(driver);
        DomainSettingPage domainSettingPage = attacksPage.editLinkClick();
        domainSettingPage.removeButtonClick();
        domainSettingPage.confirmButtonClick();
        domainSettingPage.waitUntilRemove();
        SitesPage sitesPage = new SitesPage(driver);
        assertFalse(sitesPage.isWafGraphicsExist());
    }

    @Test
    public void addAllWafSettings() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        SitesPage sitesPage = loginPage.correctLogin(adminLogin, adminPassword);
        assertFalse(sitesPage.isWafGraphicsExist());
        DomainSettingPage domainSettingPage = sitesPage.createSiteButtonClick();
        domainSettingPage.typeDomainName(siteDomain);
        domainSettingPage.selectServerIp(0);
        domainSettingPage.enabledCheckbox();
        domainSettingPage.typeSiteIpInput(siteIp);
        domainSettingPage.sendAccessLogsToSyslogClick();
        domainSettingPage.proxyToHttpsClick();
        domainSettingPage.selectWafMode(2);//Блокировка
        domainSettingPage.perimetrCheckboxClick();
        domainSettingPage.selectBalancingMode(2);//IP-Hash
        domainSettingPage.enableHttps();
        domainSettingPage.addSslCert(new File("src/main/resources/dwvaloc.crt"));
        domainSettingPage.addSslKey(new File("src/main/resources/dwvaloc.key"));
        domainSettingPage.httpsRedirectCheckboxClick();
        domainSettingPage.typeRedirectPort("8080");
        domainSettingPage.httpsProxyCheckboxClick();
        domainSettingPage.analyzeServerResponseCheckboxClick();
        domainSettingPage.add403Page(new File("src/main/resources/403.html"));
        AttacksPage attacksPage = domainSettingPage.saveButtonClick();
        attacksPage.waitUntilSave();
    }

    @Test
    public void addMinimalSettings() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        SitesPage sitesPage = loginPage.correctLogin(adminLogin, adminPassword);
        assertFalse(sitesPage.isWafGraphicsExist());
        DomainSettingPage domainSettingPage = sitesPage.createSiteButtonClick();
        domainSettingPage.typeDomainName(siteDomain);
        domainSettingPage.selectServerIp(0);
        domainSettingPage.enabledCheckbox();
        domainSettingPage.typeSiteIpInput(siteIp);
        AttacksPage attacksPage = domainSettingPage.saveButtonClick();
        attacksPage.waitUntilSave();
    }

    @Test
    public void addHttpsDomain() throws InterruptedException {
        addHttpsWafSettings();
    }

    @Ignore
    public void bug909_CustomPage403Create() throws IOException {
        SSHClient ssh = new SSHClient();
        ssh.addHostKeyVerifier(new PromiscuousVerifier());
        ssh.loadKnownHosts();
        ssh.connect(siteIp);
        ssh.authPassword("root", "akroot");
        Session session = ssh.startSession();
        Session.Command cmd = session.exec("ls /etc/nginx/custom-pages/");
        assertEquals("", IOUtils.readFully(cmd.getInputStream()).toString());
        session.close();
        ssh.disconnect();
    }

    @After
    public void tearDown() throws InterruptedException {
        removeDomain();
    }
}
