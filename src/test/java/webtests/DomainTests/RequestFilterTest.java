package webtests.DomainTests;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pages.DomainPages.AttacksPage;
import pages.DomainPages.RequestFilterPage;
import webtests.FunctionalTest;

import static io.restassured.RestAssured.given;
import static java.lang.Thread.sleep;
import static org.junit.Assert.assertTrue;


public class RequestFilterTest extends FunctionalTest {
    private void checkAllRequestPassed() {
        // Empty Request
        given().get().then().statusCode(200);
        // Cookie
        given().cookies("AUTH_COOKIE", "test").when().get().then().statusCode(200);
        // Referer
        given().header("Referer", "http://example.com/bot.html").when().get().then().statusCode(200);
        // User-Agent
        given().header("User-Agent", "msnbot").when().get().then().statusCode(200);
        // HEAD
        given().when().head().then().statusCode(200);
        // TODO: Protocol
        // URI
        given().when().get("/login2.php").then().statusCode(404);
    }

    @Before
    public void InitRestAssuredConnection() {
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri("https://" + siteDomain)
                .setPort(443)
                .setRelaxedHTTPSValidation()
                .build();
    }

    @Test
    public void allFieldExist() throws InterruptedException {
        AddDomainTest.addHttpsWafSettings();
        AttacksPage attacksPage = new AttacksPage(driver);
        RequestFilterPage requestFilterPage = attacksPage.filterRequestLinkClick();

        assertTrue(requestFilterPage.isRequestTypeSelectExist());
        assertTrue(requestFilterPage.isRegexInputExist());
        assertTrue(requestFilterPage.isAddButtonExist());
    }

    @Test
    public void testWithRequest() throws InterruptedException {
        AddDomainTest.addHttpsWafSettings();
        // Wait until nginx restart
        sleep(10000);

        this.checkAllRequestPassed();

        AttacksPage attacksPage = new AttacksPage(driver);
        RequestFilterPage requestFilterPage = attacksPage.filterRequestLinkClick();

        requestFilterPage.addNewFilter(1, "AUTH_COOKIE=([a-z0-9]+)");
        requestFilterPage.addNewFilter(2, "(https?:\\/\\/)?([\\w\\d]+\\.)?[\\w\\d]+\\.\\w+\\/?.+");
        requestFilterPage.addNewFilter(3, "msnbot|scrapbot");
        requestFilterPage.addNewFilter(4, "HEAD");
        requestFilterPage.addNewFilter(6, "/login2.php");

        // Empty Request
        given().get().then().statusCode(200);
        // Cookie
        given().cookies("AUTH_COOKIE", "test").when().get().then().statusCode(403);
        // Referer
        given().header("Referer", "http://example.com/bot.html").when().get().then().statusCode(403);
        // User-Agent
        given().header("User-Agent", "msnbot").when().get().then().statusCode(403);
        // HEAD
        given().when().head().then().statusCode(403);
        // URI
        given().when().get("/login2.php").then().statusCode(403);

        while (requestFilterPage.getRemoveButtonList().size() > 0) {
            requestFilterPage.removeFilter(0);
        }

        // Wait until nginx restart
        sleep(10000);
        this.checkAllRequestPassed();
    }

    @After
    public void tearDown() throws InterruptedException {
        RequestFilterPage requestFilterPage = new RequestFilterPage(driver);
        requestFilterPage.closeLabelClick();
        AddDomainTest.removeDomain();
    }
}
