package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
    private WebDriver driver;
    private By loginInput = By.xpath("//input[@id=\"login-form-login\"]");
    private By passwordInput = By.xpath("//input[@id=\"login-form-password\"]");
    private By rememberMeCheckbox = By.xpath("//input[@id=\"login-form-rememberme\"]");
    private By forgotLink = By.xpath("//a[@href=\"/user/forgot\"]");
    private By submitButton = By.xpath("//*[@id=\"login-form\"]/button");

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public LoginPage typeLogin(String login) {
        driver.findElement(loginInput).sendKeys(login);
        return this;
    }

    public LoginPage passwordInput(String password) {
        driver.findElement(passwordInput).sendKeys(password);
        return this;
    }

    public MainHeader submitButtonClick() {
        driver.findElement(submitButton).click();
        return new MainHeader(driver);
    }

    public SitesPage correctLogin(String login, String password) {
        driver.findElement(loginInput).sendKeys(login);
        driver.findElement(passwordInput).sendKeys(password);
        driver.findElement(submitButton).click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleIs("Сайты"));
        return new SitesPage(driver);
    }
}
