package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BackupPage {
    WebDriver driver;
    private By createBackupButton = By.xpath("/html/body/div/div[1]/div/section/div/div[1]/div[2]/button[3]");
    private By allTr = By.xpath("/html/body/div/div[1]/div/section/div[2]/div[2]/table/tbody[1]");


    public BackupPage(WebDriver driver) {
        this.driver = driver;
    }

    public BackupPage createBackupButtonClick() {
        driver.findElement(createBackupButton).click();
        driver.switchTo().alert().accept();
        return this;
    }
}
