package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class EditUserPage {
    WebDriver driver;

    public EditUserPage(WebDriver driver) {
        this.driver = driver;
    }

    private By alert = By.xpath("/html/body/div/div[1]/div/section/div[3]/div/div");
    private By delete = By.xpath("//*[@id=\"w3\"]/li[2]/a");
    private By nameInput = By.xpath("//input[@id=\"user-username\"]");
    private By emailInput = By.xpath("//input[@id=\"user-email\"]");
    private By changePassCheckbox = By.xpath("//*[@id=\"w0\"]/div[4]/div/label/span");
    private By passwordInput = By.xpath("//*[@id=\"user-password\"]");
    private By roleSelect = By.xpath("//*[@id=\"user-roles\"]");
    private By updateButton = By.xpath("//*[@id=\"w0\"]/div[6]/div/button");
    private By userLink = By.xpath("//*[@id=\"w1\"]/li[1]/a");

    public boolean isPassDisabled() {
        return driver.findElement(passwordInput).getAttribute("disabled") != null;
    }

    public String getAlertText() {
        return driver.findElement(alert).getText();
    }

    public UserPage deleteClick() {
        driver.findElement(delete).click();
        driver.switchTo().alert().accept();
        return new UserPage(driver);
    }

    public EditUserPage changePassCheckboxClick() {
        driver.findElement(changePassCheckbox).click();
        return this;
    }

    public EditUserPage selectRole(int index) {
        driver.findElement(roleSelect).findElement(By.xpath(String.format("option[%s]", index + 1))).click();
        return this;
    }

    public EditUserPage updateButtonClick() {
        driver.findElement(updateButton).click();
        return this;
    }

    public EditUserPage setName(String name) {
        driver.findElement(nameInput).clear();
        driver.findElement(nameInput).sendKeys(name);
        return this;
    }

    public EditUserPage setEmail(String email) {
        driver.findElement(emailInput).clear();
        driver.findElement(emailInput).sendKeys(email);
        return this;
    }

    public EditUserPage setPassword(String password) {
        driver.findElement(passwordInput).sendKeys(password);
        return this;
    }

    public UserPage userLinkClick() {
        driver.findElement(userLink).click();
        return new UserPage(driver);
    }
}
