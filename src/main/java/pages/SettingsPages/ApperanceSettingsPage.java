package pages.SettingsPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ApperanceSettingsPage {
    private WebDriver driver;
    private By languageSelect = By.xpath("//*[@id=\"settingsform-language\"]");
    private By colorSelect = By.xpath("//*[@id=\"settingsform-theme_color\"]");
    private By saveButton = By.xpath("//button[@type=\"submit\"]");

    ApperanceSettingsPage(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isLanguageSelectExist() {
        return driver.findElements(languageSelect).size() > 0;
    }

    public boolean isColorSelectExist() {
        return driver.findElements(colorSelect).size() > 0;
    }

    public ApperanceSettingsPage saveButtonClick() {
        driver.findElement(saveButton).click();
        return this;
    }

    public ApperanceSettingsPage selectLanguage(int index) {
        WebElement selectElem = driver.findElement(languageSelect);
        Select select = new Select(selectElem);
        select.selectByIndex(index);
        return this;
    }

    public ApperanceSettingsPage selectColor(int index) {
        WebElement selectElem = driver.findElement(colorSelect);
        Select select = new Select(selectElem);
        select.selectByIndex(index);
        return this;
    }

}
