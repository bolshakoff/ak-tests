package pages.SettingsPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SystemSettingsPage {
    private WebDriver driver;
    private By postSizeInput = By.xpath("//*[@id=\"settingsform-clientmaxbodysizeform\"]");
    private By logFilterTimeInput = By.xpath("//*[@id=\"settingsform-log_filter_time\"]");
    private By logStorageTimeInput = By.xpath("//*[@id=\"settingsform-log_storage_time\"]");
    private By licenseTextArea = By.xpath("//*[@id=\"settingsform-license_key\"]");
    private By checkIntegrity = By.xpath("//a[@ng-click=\"checkIntegrity();\"]");
    private By checkIntegrityStatusText = By.xpath("//span[@ng-show=\"integrityStatus === 'valid'\"]");
    private By licenseTypeText = By.xpath("//*[@id=\"w1\"]/div[5]/div/div");
    private By saveButton = By.xpath("//button[@type=\"submit\"]");
    private By postSizeError = By.xpath("//*[@id=\"w1\"]/div[1]/div/div[2]/p");
    private By logFilterTimeError = By.xpath("//*[@id=\"w1\"]/div[2]/div[2]/p");
    private By logStorageTimeError = By.xpath("//*[@id=\"w1\"]/div[3]/div[2]/p");
    private By successDiv = By.xpath("//div[@id=\"w1\"]");

    SystemSettingsPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getSuccessDivText() {
        return driver.findElement(successDiv).getText();
    }

    public String getIntegrityText() {
        return driver.findElement(checkIntegrityStatusText).getText();
    }

    public SystemSettingsPage saveButtonClick() {
        driver.findElement(saveButton).click();
        return this;
    }

    public SystemSettingsPage checkIntegriryClick() {
        driver.findElement(checkIntegrity).click();
        return this;
    }

    public String getPostSizeErrorText() {
        return driver.findElement(postSizeError).getText();
    }

    public String getLogFilterTimeErrorText() {
        return driver.findElement(logFilterTimeError).getText();
    }

    public String getLogStorageTimeErrorText() {
        return driver.findElement(logStorageTimeError).getText();
    }

    public int getPostSize() {
        return Integer.parseInt(driver.findElement(postSizeInput).getAttribute("value"));
    }

    public SystemSettingsPage typePostSize(String size) {
        driver.findElement(postSizeInput).clear();
        driver.findElement(postSizeInput).sendKeys(size);
        return this;
    }

    public int getLogStorageTimeInput() {
        return Integer.parseInt(driver.findElement(logStorageTimeInput).getAttribute("value"));
    }

    public SystemSettingsPage typeLogFilterTimeInput(String time) {
        driver.findElement(logFilterTimeInput).clear();
        driver.findElement(logFilterTimeInput).sendKeys(time);
        return this;
    }

    public SystemSettingsPage typeLogStorageTimeInput(String time) {
        driver.findElement(logStorageTimeInput).clear();
        driver.findElement(logStorageTimeInput).sendKeys(time);
        return this;
    }

    public boolean isPostSizeInputExist() {
        return driver.findElements(postSizeInput).size() > 0;
    }

    public boolean isPostSizeErrorExist() {
        return driver.findElements(postSizeError).size() > 0;
    }

    public boolean isLogFilterTimeExist() {
        return driver.findElements(logFilterTimeInput).size() > 0;
    }

    public boolean isLogStorageTimeExist() {
        return driver.findElements(logStorageTimeInput).size() > 0;
    }

    public boolean isLogStorageTimeErrorExist() {
        return driver.findElements(logStorageTimeError).size() > 0;
    }

    public boolean isLicenseTextareaExist() {
        return driver.findElements(licenseTextArea).size() > 0;
    }

    public boolean isCheckIntegrityExist() {
        return driver.findElements(checkIntegrity).size() > 0;
    }

    public boolean isCheckIntegrityTextExist() {
        return driver.findElements(checkIntegrity).size() > 0;
    }

    public boolean isLicenseTypeTextExist() {
        return driver.findElements(licenseTypeText).size() > 0;
    }

    public String getLicenseType() {
        return driver.findElement(licenseTypeText).getText();
    }
}
