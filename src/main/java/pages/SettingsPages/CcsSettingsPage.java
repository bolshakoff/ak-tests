package pages.SettingsPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CcsSettingsPage {
    WebDriver driver;
    private By memoryInput = By.xpath("//*[@id=\"settingsform-available_ram_for_appercut\"]");
    private By memoryErrorText = By.xpath("//*[@id=\"w1\"]/div[1]/div[2]/p");
    private By runtimeTimeoutInput = By.xpath("//*[@id=\"settingsform-appercut_runtime_timeout\"]");
    private By saveButton = By.xpath("//button[@type=\"submit\"]");

    // TODO: Добавить проверку кнопки "Изменить статусы уязвимостей"


    CcsSettingsPage(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isMemoryInputExist() {
        return driver.findElements(memoryInput).size() > 0;
    }

    public String getMemoryErrorText() {
        return driver.findElement(memoryErrorText).getText();
    }

    public boolean isRuntimeTimeoutInputExist() {
        return driver.findElements(runtimeTimeoutInput).size() > 0;
    }

    public CcsSettingsPage setMemoryInput(String memory) {
        driver.findElement(memoryInput).clear();
        driver.findElement(memoryInput).sendKeys(memory);
        return this;
    }

    public boolean isSaveButtonExist() {
        return driver.findElements(saveButton).size() > 0;
    }

    public CcsSettingsPage saveButtonClick() {
        driver.findElement(saveButton).click();
        return this;
    }
}
