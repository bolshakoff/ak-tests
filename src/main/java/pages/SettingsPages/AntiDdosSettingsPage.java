package pages.SettingsPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AntiDdosSettingsPage {
    WebDriver driver;
    private By ratioInput = By.xpath("//*[@id=\"settingsform-allowed_qrator_traffic_ratio\"]");
    private By durationInput = By.xpath("//*[@id=\"settingsform-min_qrator_attack_duration_for_report\"]");
    private By minTrafficinput = By.xpath("//*[@id=\"settingsform-min_trafic_for_fix_attack\"]");
    private By fromAntiDdosCheckbox = By.xpath("//*[@id=\"w1\"]/div[4]/label");

    public AntiDdosSettingsPage(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isRatioInputExist() {
        return driver.findElements(ratioInput).size() > 0;
    }

    public boolean isDurationInputExist() {
        return driver.findElements(durationInput).size() > 0;
    }

    public boolean isMinTrafficInputExist() {
        return driver.findElements(minTrafficinput).size() > 0;
    }

    public boolean isFromAntiDdosCheckboxExist() {
        return driver.findElements(fromAntiDdosCheckbox).size() > 0;
    }
}
