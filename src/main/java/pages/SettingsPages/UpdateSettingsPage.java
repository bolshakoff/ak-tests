package pages.SettingsPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class UpdateSettingsPage {
    private WebDriver driver;
    private By autoUpdateCheckbox = By.xpath("//*[@id=\"w1\"]/div[1]/div/label/span");
    private By currentVersionText = By.xpath("//*[@id=\"w1\"]/div[1]/div/div/span");
    private By saveButton = By.xpath("//button[@type=\"submit\"]");
    private By successDiv = By.xpath("//div[@id=\"w1\"]");


    UpdateSettingsPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getCurrentVersion() {
        return driver.findElement(currentVersionText).getText();
    }

    public String getSuccessDivText() {
        return driver.findElement(successDiv).getText();
    }

    public boolean isAutoUpdateCheckboxExist() {
        return driver.findElements(autoUpdateCheckbox).size() > 0;
    }

    public boolean isCurrentVersionTextExist() {
        return driver.findElements(currentVersionText).size() > 0;
    }

    public boolean isSaveButtonExist() {
        return driver.findElements(saveButton).size() > 0;
    }

    public UpdateSettingsPage saveButtonClick() {
        driver.findElement(saveButton).click();
        return this;
    }

    public UpdateSettingsPage autoUpdateCheckboxClick() {
        driver.findElement(autoUpdateCheckbox).click();
        return this;
    }

}
