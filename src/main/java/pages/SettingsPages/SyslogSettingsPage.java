package pages.SettingsPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SyslogSettingsPage {
    WebDriver driver;
    private By syslogEnableCheckbox = By.xpath("//*[@id=\"w1\"]/div[1]/div/label/span");
    private By syslogIpInput = By.xpath("//*[@id=\"settingsform-syslog_servers\"]");
    private By saveButton = By.xpath("//button[@type=\"submit\"]");
    private By successDiv = By.xpath("//div[@id=\"w1\"]");

    SyslogSettingsPage(WebDriver driver) {
        this.driver = driver;
    }

    public SyslogSettingsPage setSyslogIpInput(String ip) {
        driver.findElement(syslogIpInput).clear();
        driver.findElement(syslogIpInput).sendKeys(ip);
        return this;
    }

    public String getSuccessDivText() {
        return driver.findElement(successDiv).getText();
    }

    public boolean isSaveButtonExist() {
        return driver.findElements(saveButton).size() > 0;
    }

    public SyslogSettingsPage syslogEnableCheckboxClick() {
        driver.findElement(syslogEnableCheckbox).click();
        return this;
    }

    public SyslogSettingsPage saveButtonClick() {
        driver.findElement(saveButton).click();
        return this;
    }

    public boolean isSyslogEnableCheckboxExist() {
        return driver.findElements(syslogEnableCheckbox).size() > 0;
    }

    public boolean isSyslogIpInputExist() {
        return driver.findElements(syslogIpInput).size() > 0;
    }
}
