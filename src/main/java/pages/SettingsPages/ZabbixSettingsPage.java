package pages.SettingsPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ZabbixSettingsPage {
    WebDriver driver;
    private By ipInput = By.xpath("//input[@id=\"settingsform-zabbix_ip\"]");
    private By portinput = By.xpath("//input[@id=\"settingsform-zabbix_port\"]");
    private By saveButton = By.xpath("//button[@type=\"submit\"]");

    ZabbixSettingsPage(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isIpInputExist() {
        return driver.findElements(ipInput).size() > 0;
    }

    public boolean isPortinputExist() {
        return driver.findElements(portinput).size() > 0;
    }

    public boolean isSaveButtonExist() {
        return driver.findElements(saveButton).size() > 0;
    }
}
