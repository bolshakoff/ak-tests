package pages.SettingsPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MasterSlaveSettingsPage {
    private WebDriver driver;
    private By masterIpInput = By.xpath("//*[@id=\"settingsform-master_node_ip\"]");
    private By connectButton = By.xpath("//*[@id=\"w1\"]/div[2]/div/button");
    private By apiKeyInput = By.xpath("//*[@id=\"settingsform-api_key\"]");
    private By apiSyncPortInput = By.xpath("//*[@id=\"settingsform-api_sync_port\"]");
    private By saveButton = By.xpath("//button[@type=\"submit\"]");

    MasterSlaveSettingsPage(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isApiKeyInputExist() {
        return driver.findElements(apiKeyInput).size() > 0;
    }

    public boolean isConnectButtonExist() {
        return driver.findElements(connectButton).size() > 0;
    }

    public boolean isMasterIpInputExist() {
        return driver.findElements(masterIpInput).size() > 0;
    }

    public boolean isApiSyncPortInputExist() {
        return driver.findElements(apiSyncPortInput).size() > 0;
    }

    public boolean isSaveButtonExist() {
        return driver.findElements(saveButton).size() > 0;
    }

    public MasterSlaveSettingsPage saveButtonClick() {
        driver.findElement(saveButton).click();
        return this;
    }
}
