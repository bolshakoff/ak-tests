package pages.SettingsPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import static java.lang.Thread.sleep;

public class MailSettingsPage {
    WebDriver driver;
    private By hostEmailInput = By.xpath("//*[@id=\"settingsform-noreply_email\"]");
    private By hostEmailWarning = By.xpath("//*[@id=\"w1\"]/div[1]/div[2]/p");
    private By hostInput = By.xpath("//*[@id=\"settingsform-host\"]");
    private By hostWarning = By.xpath("//*[@id=\"w1\"]/div[2]/div[2]/p");
    private By usernameInput = By.xpath("//*[@id=\"settingsform-username\"]");
    private By passwordInput = By.xpath("//*[@id=\"settingsform-password\"]");
    private By portInput = By.xpath("//*[@id=\"settingsform-port\"]");
    private By encryptionSelect = By.xpath("//*[@id=\"settingsform-encryption\"]");
    private By validateSertificateCheckbox = By.xpath("//*[@id=\"w1\"]/div[7]/div/label/span");
    private By saveButton = By.xpath("//button[@type=\"submit\"]");
    private By successDiv = By.xpath("//div[@id=\"w1\"]");
    private By checkButton = By.xpath("//a[@ng-click=\"check()\"]");
    private By checkButton2 = By.xpath("/html/body/div[2]/div/div/div[3]/button[1]");
    private By checkStatusText = By.xpath("/html/body/div[2]/div/div/div[2]/div");


    MailSettingsPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getSuccessDivText() {
        return driver.findElement(successDiv).getText();
    }

    public String getCheckStatusText() throws InterruptedException {
        sleep(500);
        String str = driver.findElement(checkStatusText).getText();
        driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/button")).click();
        return str;
    }

    public boolean isHostEmailInputExist() {
        return driver.findElements(hostEmailInput).size() > 0;
    }

    public boolean isHostInputExist() {
        return driver.findElements(hostInput).size() > 0;
    }

    public boolean isUsernameInputExist() {
        return driver.findElements(usernameInput).size() > 0;
    }

    public boolean isPasswordInputExist() {
        return driver.findElements(passwordInput).size() > 0;
    }

    public boolean isEncryptionSelectExist() {
        return driver.findElements(encryptionSelect).size() > 0;
    }

    public boolean isSaveButtonExist() {
        return driver.findElements(saveButton).size() > 0;
    }

    public boolean isCheckButtonExist() {
        return driver.findElements(checkButton).size() > 0;
    }

    public boolean isValidateSertificateCheckboxExist() {
        return driver.findElements(validateSertificateCheckbox).size() > 0;
    }

    public MailSettingsPage validateSertificateCheckboxClick() {
        driver.findElement(validateSertificateCheckbox).click();
        return this;
    }

    public MailSettingsPage saveButtonClick() {
        driver.findElement(saveButton).click();
        return this;
    }

    public MailSettingsPage checkButtonClick() {
        driver.findElement(checkButton).click();
        return this;
    }

    public MailSettingsPage checkButton2Click() {
        driver.findElement(checkButton2).click();
        return this;
    }

    public String getHostEmailWarning(){
        //TODO return ""
        return driver.findElement(hostEmailWarning).getText();
    }

    public String getHostWarning(){
        //TODO return ""
        return driver.findElement(hostWarning).getText();
    }

    public MailSettingsPage typeHostEmail(String email){
        driver.findElement(hostEmailInput).clear();
        driver.findElement(hostEmailInput).sendKeys(email);
        return this;
    }

    public MailSettingsPage typeHost(String host){
        driver.findElement(hostInput).clear();
        driver.findElement(hostInput).sendKeys(host);
        return this;
    }

    public MailSettingsPage typeUsername(String username){
        driver.findElement(usernameInput).clear();
        driver.findElement(usernameInput).sendKeys(username);
        return this;
    }

    public MailSettingsPage typePassword(String password){
        driver.findElement(passwordInput).clear();
        driver.findElement(passwordInput).sendKeys(password);
        return this;
    }

    public MailSettingsPage typePort(String port){
        driver.findElement(portInput).clear();
        driver.findElement(portInput).sendKeys(port);
        return this;
    }

    public MailSettingsPage selectEncryption(int index) {
        WebElement selectElem = driver.findElement(encryptionSelect);
        Select select = new Select(selectElem);
        select.selectByIndex(index);
        return this;
    }

    public String getAlertText(){
        return driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/div")).getText();
    }
}
