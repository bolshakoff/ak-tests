package pages.SettingsPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ReportSettingsPage {
    WebDriver driver;
    private By weeklyReportCheckbox = By.xpath("//*[@id=\"w1\"]/div[1]/div/label/span");
    private By vulnerabilityDetectionCheckbox = By.xpath("//*[@id=\"w1\"]/div[2]/div/label/span");
    private By attacksSelectionCheckbox = By.xpath("//*[@id=\"w1\"]/div[3]/div/label/span");
    private By attacksIntervalInput = By.xpath("//*[@id=\"settingsform-attacks_detection_interval\"]");
    private By loginReportCheckbox = By.xpath("//*[@id=\"w1\"]/div[5]/div/label/span");
    private By unauthorizedCheckbox = By.xpath("//*[@id=\"w1\"]/div[6]/div/label/span");
    private By saveButton = By.xpath("//button[@type=\"submit\"]");
    private By successDiv = By.xpath("//div[@id=\"w1\"]");

    ReportSettingsPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getSuccessDivText() {
        return driver.findElement(successDiv).getText();
    }

    public ReportSettingsPage setAttacksIntervalInput(String interval) {
        driver.findElement(attacksIntervalInput).clear();
        driver.findElement(attacksIntervalInput).sendKeys(interval);
        return this;
    }

    public boolean isWeeklyReportCheckboxExist() {
        return driver.findElements(weeklyReportCheckbox).size() > 0;
    }

    public boolean isVulnerabilityDetectionCheckboxExist() {
        return driver.findElements(vulnerabilityDetectionCheckbox).size() > 0;
    }

    public boolean isAttacksSelectionCheckboxExist() {
        return driver.findElements(attacksSelectionCheckbox).size() > 0;
    }

    public boolean isAttacksIntervalInputExist() {
        return driver.findElements(attacksIntervalInput).size() > 0;
    }

    public boolean isLoginReportCheckboxExist() {
        return driver.findElements(loginReportCheckbox).size() > 0;
    }

    public boolean isUnauthorizedCheckboxExist() {
        return driver.findElements(unauthorizedCheckbox).size() > 0;
    }

    public boolean isSaveButtonExist() {
        return driver.findElements(saveButton).size() > 0;
    }

    public ReportSettingsPage saveButtonClick() {
        driver.findElement(saveButton).click();
        return this;
    }

    public ReportSettingsPage attacksSetectionCheckboxClick() {
        driver.findElement(attacksSelectionCheckbox).click();
        return this;
    }

    public ReportSettingsPage loginReportCheckboxClick() {
        driver.findElement(loginReportCheckbox).click();
        return this;
    }

    public ReportSettingsPage unautorizedCheckboxClick() {
        driver.findElement(unauthorizedCheckbox).click();
        return this;
    }
}
