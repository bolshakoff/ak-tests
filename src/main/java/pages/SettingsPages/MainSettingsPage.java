package pages.SettingsPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainSettingsPage {
    private WebDriver driver;
    private By apperanceLink = By.xpath("//a[@href=\"/settings/default/appearance\"]");
    private By systemLink = By.xpath("//a[@href=\"/settings/default/system\"]");
    private By masterSlaveLink = By.xpath("//a[@href=\"/settings/default/master-slave\"]");
    private By updateLink = By.xpath("//a[@href=\"/settings/default/update\"]");
    private By reportsLink = By.xpath("//a[@href=\"/settings/default/reports\"]");
    private By syslogLink = By.xpath("//a[@href=\"/settings/default/logs\"]");
    private By mailLink = By.xpath("//a[@href=\"/settings/default/mail\"]");
    private By backupLink = By.xpath("//a[@href=\"/settings/default/backup\"]");
    private By securityLink = By.xpath("//a[@href=\"/settings/default/security\"]");
    private By antiddosLink = By.xpath("//a[@href=\"/settings/default/antiddos\"]");
    private By wafLink = By.xpath("//a[@href=\"/settings/default/waf\"]");
    private By ccsLink = By.xpath("//a[@href=\"/settings/default/ccs\"]");
    private By zabbixLink = By.xpath("//a[@href=\"/settings/default/zabbix\"]");
    private By networkLink = By.xpath("//a[@href=\"/settings/default/network\"]");


    public MainSettingsPage(WebDriver driver) {
        this.driver = driver;
    }

    public ApperanceSettingsPage apperanceLinkClick() {
        driver.findElement(apperanceLink).click();
        return new ApperanceSettingsPage(driver);
    }

    public ApperanceSettingsPage backupLinkClick() {
        driver.findElement(backupLink).click();
        return new ApperanceSettingsPage(driver);
    }

    public MasterSlaveSettingsPage masterSlaveLinkClick() {
        driver.findElement(masterSlaveLink).click();
        return new MasterSlaveSettingsPage(driver);
    }

    public UpdateSettingsPage updateLinkClick() {
        driver.findElement(updateLink).click();
        return new UpdateSettingsPage(driver);
    }

    public ReportSettingsPage reportsLinkClick() {
        driver.findElement(reportsLink).click();
        return new ReportSettingsPage(driver);
    }

    public SyslogSettingsPage syslogLinkClick() {
        driver.findElement(syslogLink).click();
        return new SyslogSettingsPage(driver);
    }

    public NetworkSettingPage networkLinkClick() {
        driver.findElement(networkLink).click();
        return new NetworkSettingPage(driver);
    }

    public SecuritySettingsPage securityLinkClick() {
        driver.findElement(securityLink).click();
        return new SecuritySettingsPage(driver);
    }

    public SystemSettingsPage systemLinkClick() {
        driver.findElement(systemLink).click();
        return new SystemSettingsPage(driver);
    }

    public CcsSettingsPage ccsLinkClick() {
        driver.findElement(ccsLink).click();
        return new CcsSettingsPage(driver);
    }

    public WafSettingsPage wafLinkClick() {
        driver.findElement(wafLink).click();
        return new WafSettingsPage(driver);
    }

    public AntiDdosSettingsPage antiddosLinkClick() {
        driver.findElement(antiddosLink).click();
        return new AntiDdosSettingsPage(driver);
    }

    public MailSettingsPage mailLinkClick() {
        driver.findElement(mailLink).click();
        return new MailSettingsPage(driver);
    }

    public ZabbixSettingsPage zabbixLinkClick() {
        driver.findElement(zabbixLink).click();
        return new ZabbixSettingsPage(driver);
    }
}
