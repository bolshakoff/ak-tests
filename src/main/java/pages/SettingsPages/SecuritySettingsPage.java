package pages.SettingsPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SecuritySettingsPage {
    WebDriver driver;
    private By minPassInput = By.xpath("//*[@id=\"settingsform-password_min_length\"]");
    private By upperLowerCheckbox = By.xpath("//*[@id=\"w1\"]/div[2]/div/label/span");
    private By numbersCheckbox = By.xpath("//*[@id=\"w1\"]/div[3]/div/label/span");
    private By specialCharCheckbox = By.xpath("//*[@id=\"w1\"]/div[4]/div/label/span");
    private By intervalInput = By.xpath("//*[@id=\"settingsform-security_brute_interval\"]");
    private By attemptsInput = By.xpath("//*[@id=\"settingsform-security_brute_allowable_attempts\"]");
    private By durationInput = By.xpath("//*[@id=\"settingsform-security_brute_block_minutes\"]");
    private By sertificateFileInput = By.xpath("//*[@id=\"settingsform-certificate\"]");
    private By keyFileInput = By.xpath("//*[@id=\"settingsform-key\"]");

    public SecuritySettingsPage(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isMinPassInputExist() {
        return driver.findElements(minPassInput).size() > 0;
    }

    public boolean isUpperLowerCheckboxExist() {
        return driver.findElements(upperLowerCheckbox).size() > 0;
    }

    public boolean isNnumbersCheckboxExist() {
        return driver.findElements(numbersCheckbox).size() > 0;
    }

    public boolean isSpecialCharCheckboxExist() {
        return driver.findElements(specialCharCheckbox).size() > 0;
    }

    public boolean isIntervalInputExist() {
        return driver.findElements(intervalInput).size() > 0;
    }

    public boolean isAttemptsInputExist() {
        return driver.findElements(attemptsInput).size() > 0;
    }

    public boolean isDurationInputExist() {
        return driver.findElements(durationInput).size() > 0;
    }

    public boolean isSertificateFileInputExist() {
        return driver.findElements(sertificateFileInput).size() > 0;
    }

    public boolean isKeyFileInputExist() {
        return driver.findElements(keyFileInput).size() > 0;
    }
}
