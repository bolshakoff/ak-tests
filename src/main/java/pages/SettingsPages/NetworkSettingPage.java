package pages.SettingsPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class NetworkSettingPage {
    WebDriver driver;
    private By natCheckbox = By.xpath("//*[@id=\"w1\"]/div[1]/div/label/span");
    private By tcpPortsInput = By.xpath("//*[@id=\"settingsform-tcp_ports\"]");
    private By udpPortsInput = By.xpath("//*[@id=\"settingsform-udp_ports\"]");
    private By ipv6Checkbox = By.xpath("//*[@id=\"w1\"]/div[4]/div/label/span");
    private By webCheckbox = By.xpath("//*[@id=\"w1\"]/div[5]/div[1]/div/label/span");
    private By saveButton = By.xpath("//button[@type=\"submit\"]");

    NetworkSettingPage(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isNatCheckboxExist() {
        return driver.findElements(natCheckbox).size() > 0;
    }

    public boolean isTcpPortsInputExist() {
        return driver.findElements(tcpPortsInput).size() > 0;
    }

    public boolean isUdpPortsInputExist() {
        return driver.findElements(udpPortsInput).size() > 0;
    }

    public boolean isIpv6CheckboxExist() {
        return driver.findElements(ipv6Checkbox).size() > 0;
    }

    public boolean isWebCheckboxExist() {
        return driver.findElements(webCheckbox).size() > 0;
    }
    public boolean isSaveButtonExist() {
        return driver.findElements(saveButton).size() > 0;
    }

    public NetworkSettingPage saveButtonClick() {
        driver.findElement(saveButton).click();
        return this;
    }
}
