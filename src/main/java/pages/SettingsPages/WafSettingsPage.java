package pages.SettingsPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class WafSettingsPage {
    WebDriver driver;
    private By activeScannerSelect = By.xpath("//select[@id=\"waf-scanner\"]");
    private By cacheInput = By.xpath("//*[@id=\"settingsform-waf_dashboard_cache_duration\"]");
    private By attackCount = By.xpath("//*[@id=\"settingsform-waf_attacks_page_size\"]");
    private By banIpDuration = By.xpath("//*[@id=\"settingsform-ban_ip_from_waf_duration\"]");

    public WafSettingsPage(WebDriver driver) {
        this.driver = driver;
    }

    public boolean activeScannerSelectExist() {
        return driver.findElements(activeScannerSelect).size() > 0;
    }

    public boolean isCacheInputExist() {
        return driver.findElements(cacheInput).size() > 0;
    }

    public boolean isAttackCountExist() {
        return driver.findElements(attackCount).size() > 0;
    }

    public boolean isBanIpDurationExist() {
        return driver.findElements(banIpDuration).size() > 0;
    }
}
