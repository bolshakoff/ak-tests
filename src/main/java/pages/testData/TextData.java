package pages.testData;

public class TextData {

    static String[] getNegativeStringArray() {
        return new String[]{" ", "''", " a a ", " 1 1 ", "<script type='text/javascript'>alert('xss');</script>", "123.123.123,00", "“[|]’~< !–@/*$%^&#*/()?>,.*/\\", "\u0001"};
    }
}
