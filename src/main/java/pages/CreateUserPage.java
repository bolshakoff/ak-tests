package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CreateUserPage {
    WebDriver driver;
    private By userNameInput = By.xpath("//input[@id=\"user-username\"]");
    private By emailInput = By.xpath("//input[@id=\"user-email\"]");
    private By passwordInput = By.xpath("//input[@id=\"user-password\"]");
    private By roleInput = By.xpath("//select[@id=\"user-roles\"]");
    private By saveButton = By.xpath("//button[@type=\"submit\"]");

    public CreateUserPage(WebDriver driver) {
        this.driver = driver;
    }

    public CreateUserPage setUserName(String name) {
        driver.findElement(userNameInput).sendKeys(name);
        return this;
    }

    public CreateUserPage setEmail(String email) {
        driver.findElement(emailInput).sendKeys(email);
        return this;
    }

    public CreateUserPage setPassword(String password) {
        driver.findElement(passwordInput).sendKeys(password);
        return this;
    }

    public CreateUserPage selectRole(int index) {
        WebElement selectElem = driver.findElement(roleInput);
        Select select = new Select(selectElem);
        select.selectByIndex(index);
        return this;
    }

    public EditUserPage saveButtonClick() {
        driver.findElement(saveButton).click();
        return new EditUserPage(driver);
    }
}
