package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserPage {
    WebDriver driver;
    private By allTr = By.xpath("//*[@id=\"w2\"]/table/tbody");
    private By userLink = By.xpath("//*[@id=\"w1\"]/li[1]/a");
    private By rolesLink = By.xpath("//*[@id=\"w0\"]/li[2]/a");
    private By createLink = By.xpath("//*[@id=\"w0\"]/li[4]/a");
    private By createUserLink = By.xpath("//*[@id=\"w1\"]/li[1]/a");
    private By createRoleLink = By.xpath("//*[@id=\"w1\"]/li[2]/a");
    private By alert = By.xpath("/html/body/div/div[1]/div/section/div[2]/div/div");

    public String getAlertText() {
        return driver.findElement(alert).getText();
    }

    public RoleUserPage rolesLinkClick() {
        driver.findElement(rolesLink).click();
        return new RoleUserPage(driver);
    }

    public UserPage createLinkClick() {
        driver.findElement(createLink).click();
        return this;
    }

    public UserPage userLinkClick() {
        driver.findElement(userLink).click();
        return this;
    }

    public CreateUserPage createUserLinkClick() {
        driver.findElement(createUserLink).click();
        return new CreateUserPage(driver);
    }

    public CreateRolePage createRoleLinkClick() {
        driver.findElement(createRoleLink).click();
        return new CreateRolePage(driver);
    }

    public UserPage(WebDriver driver) {
        this.driver = driver;
    }

    private List<List<String>> getAllUsers() {
        List<WebElement> trList = driver.findElement(allTr).findElements(By.xpath("tr"));
        List<List<String>> usersList = new ArrayList<>();
        for (WebElement element : trList) {
            List<String> userParmsList = new ArrayList<>();
            List<WebElement> tdList = element.findElements(By.xpath("td"));
            for (WebElement elem : tdList) {
                userParmsList.add(elem.getText());
            }
            usersList.add(userParmsList);
        }
        return usersList;
    }

    public Map<String, Integer> getAllNames() {
        Map<String, Integer> nameMap = new HashMap<>();
        int count = 0;
        for (List<String> rolesList : getAllUsers()) {
            nameMap.put(rolesList.get(0), count);
            count++;
        }
        return nameMap;
    }

    public Map<String, Integer> getAllEmails() {
        Map<String, Integer> emailMap = new HashMap<>();
        int count = 0;
        for (List<String> rolesList : getAllUsers()) {
            emailMap.put(rolesList.get(1), count);
            count++;
        }
        return emailMap;
    }

    public Map<String, Integer> getAllRoles() {
        Map<String, Integer> roleMap = new HashMap<>();
        int count = 0;
        for (List<String> rolesList : getAllUsers()) {
            roleMap.put(rolesList.get(2), count);
            count++;
        }
        return roleMap;
    }

    private WebElement getUserElement(int id) {
        return driver.findElement(allTr).findElement(By.xpath(String.format("tr[%s]", id)));
    }

    public String getName(int id) {
        return getUserElement(id).findElements(By.xpath("td")).get(0).getText();
    }

    public String getEmail(int id) {
        return getUserElement(id).findElements(By.xpath("td")).get(1).getText();
    }

    public String getRole(int id) {
        return getUserElement(id).findElements(By.xpath("td")).get(2).getText();
    }

    private List<WebElement> getButtonsElementsList(int id) {
        return getUserElement(id).findElements(By.xpath("td")).get(3).findElements(By.xpath("a"));
    }

    public int getButtonCount(int id) {
        return getButtonsElementsList(id).size();
    }

    public EditUserPage editClick(int id) {
        getButtonsElementsList(id).get(0).click();
        return new EditUserPage(driver);
    }

    public UserPage blockClick(int id) {
        getButtonsElementsList(id).get(1).click();
        driver.switchTo().alert().accept();
        return this;
    }

    public UserPage deleteClick(int id) {
        getButtonsElementsList(id).get(2).click();
        driver.switchTo().alert().accept();
        return this;
    }

    public String getWarningText() {
        return driver.findElement(By.xpath("/html/body/div/div[1]/div/section/div[3]/div/div")).getText();
    }
}