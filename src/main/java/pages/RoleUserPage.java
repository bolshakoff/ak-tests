package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RoleUserPage {
    WebDriver driver;
    private By allTr = By.xpath("//*[@id=\"w2\"]/table/tbody");

    public RoleUserPage(WebDriver driver) {
        this.driver = driver;
    }

    public List<List<String>> getAllRoles() {
        List<WebElement> trList = driver.findElement(allTr).findElements(By.xpath("tr"));
        List<List<String>> rolesList = new ArrayList<>();
        for (WebElement element : trList) {
            List<String> roleParmsList = new ArrayList<>();
            List<WebElement> tdList = element.findElements(By.xpath("td"));
            for (WebElement elem : tdList) {
                roleParmsList.add(elem.getText());
            }
            rolesList.add(roleParmsList);
        }
        return rolesList;
    }

    public String getTitle() {
        return driver.getTitle();
    }

    public Map<String, Integer> getAllNames() {
        Map<String, Integer> nameMap = new HashMap<>();
        int count = 0;
        for (List<String> rolesList : getAllRoles()) {
            nameMap.put(rolesList.get(0), count);
            count++;
        }
        return nameMap;
    }

    public Map<String, Integer> getAllDescription() {
        Map<String, Integer> descMap = new HashMap<>();
        int count = 0;
        for (List<String> rolesList : getAllRoles()) {
            descMap.put(rolesList.get(1), count);
            count++;
        }
        return descMap;
    }

    private WebElement getUserElement(int id) {
        return driver.findElement(allTr).findElement(By.xpath(String.format("tr[@data-key=\"%s\"]", id)));
    }

    public String getName(int id) {
        return getUserElement(id).findElements(By.xpath("td")).get(0).getText();
    }

    public String getDescription(int id) {
        return getUserElement(id).findElements(By.xpath("td")).get(1).getText();
    }

    private List<WebElement> getButtonsElementsList(int id) {
        return getUserElement(id).findElements(By.xpath("td")).get(2).findElements(By.xpath("a"));
    }

    public boolean isButtonExist(int id) {
        return getButtonsElementsList(id).size() > 0;
    }

    public RoleUserPage deleteRole(int id) {
        getButtonsElementsList(id).get(1).click();
        driver.switchTo().alert().accept();
        return this;
    }

    public RoleUserPage editRole(int id) {
        getButtonsElementsList(id).get(0).click();
        return this;
    }

}
