package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.SettingsPages.MainSettingsPage;

public class MainHeader {
    WebDriver driver;
    private By settingsLink = By.xpath("//a[@href=\"/settings/default\"]");
    private By userLink = By.xpath("/html/body/div/div[1]/header/nav/a[3]");
    private By backupLink = By.xpath("/html/body/div/div[1]/header/nav/a[5]");
    private By logoutLink = By.xpath("/html/body/div/div[1]/header/div/a");

    public MainHeader(WebDriver driver) {
        this.driver = driver;
    }

    public MainSettingsPage settingsLinkClick() {
        driver.findElement(settingsLink).click();
        return new MainSettingsPage(driver);
    }

    public BackupPage backupLinkClick() {
        driver.findElement(backupLink).click();
        return new BackupPage(driver);
    }

    public LoginPage logoutPageClick() {
        driver.findElement(logoutLink).click();
        return new LoginPage(driver);
    }

    public UserPage userLinkClick() {
        driver.findElement(userLink).click();
        return new UserPage(driver);
    }
}
