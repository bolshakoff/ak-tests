package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AddAdminPage {
    private WebDriver driver;
    private By adminNameInput = By.xpath("//input[@id=\"addadminform-username\"]");
    private By adminEmailInput = By.xpath("//input[@id=\"addadminform-email\"]");
    private By adminPasswordInput = By.xpath(" //input[@id=\"addadminform-password\"]");
    private By adminPasswordConfirmInput = By.xpath("//input[@id=\"addadminform-passwordconfirm\"]");
    private By submitButton = By.xpath("//*[@id=\"w0\"]/footer/button");

    public AddAdminPage(WebDriver driver) {
        this.driver = driver;
    }

    public AddAdminPage typeAdminName(String adminName) {
        driver.findElement(adminNameInput).sendKeys(adminName);
        return this;
    }

    public AddAdminPage typeEmail(String email) {
        driver.findElement(adminEmailInput).sendKeys(email);
        return this;
    }

    public AddAdminPage typePassword(String password) {
        driver.findElement(adminPasswordInput).sendKeys(password);
        return this;
    }

    public AddAdminPage typePasswordConfirm(String confirmPassword) {
        driver.findElement(adminPasswordConfirmInput).sendKeys(confirmPassword);
        return this;
    }

    public LoginPage submitButtonClick() {
        driver.findElement(submitButton).click();
        return new LoginPage(driver);
    }


}
