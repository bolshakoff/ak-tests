package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class CreateRolePage {
    WebDriver driver;
    private By roleNameInput = By.xpath("//input[@id=\"role-name\"]");
    private By descriptionInput = By.xpath("//textarea[@id=\"role-description\"]");
    private By rules = By.xpath("//*[@id=\"w2\"]/div[3]/span[2]/span[1]/span/ul");
    private By rulesList = By.xpath("//*[@id=\"select2-children-results\"]");
    private By saveButton = By.xpath("//button[@type=\"submit\"]");

    public CreateRolePage(WebDriver driver) {
        this.driver = driver;
    }

    public List<String> getRulesListElements() {
        List<String> rulesStringList = new ArrayList<>();
        driver.findElement(rules).click();
        List<WebElement> rulesListElements = driver.findElement(rulesList).findElements(By.xpath("li"));
        for (WebElement elem : rulesListElements) {
            rulesStringList.add(elem.getText());
        }
        return rulesStringList;
    }

    public RoleUserPage saveButtonClick() {
        driver.findElement(saveButton).click();
        return new RoleUserPage(driver);
    }

    public CreateRolePage setRoleName(String name) {
        driver.findElement(roleNameInput).sendKeys(name);
        return this;
    }

    public CreateRolePage setDescription(String description) {
        driver.findElement(descriptionInput).sendKeys(description);
        return this;
    }

    public CreateRolePage setRule(int index) {
        driver.findElement(rules).click();
        List<WebElement> rulesListElements = driver.findElement(rulesList).findElements(By.xpath("li"));
        rulesListElements.get(index).click();
        return this;
    }
}
