package pages.AddCcsModule;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AddCcsModulePage {
    WebDriver driver;
    private By saveButton = By.xpath("/html/body/div[1]/div[1]/div[1]/section/div[3]/div/div/section/form/footer/a[1]");
    private By zipLink = By.xpath("/html/body/div[1]/div[1]/div[1]/section/div[3]/div/div/section/form/div/ul/li[1]/a");
    private By repositoryLink = By.xpath("/html/body/div[1]/div[1]/div[1]/section/div[3]/div/div/section/form/div/ul/li[2]/a");
    private By ftpLink = By.xpath("/html/body/div[1]/div[1]/div[1]/section/div[3]/div/div/section/form/div/ul/li[3]/a");

    public AddCcsModulePage(WebDriver driver) {
        this.driver = driver;
    }
}


