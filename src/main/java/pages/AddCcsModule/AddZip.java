package pages.AddCcsModule;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.io.File;

public class AddZip extends AddCcsModulePage {
    private By nameInput = By.xpath("//*[@id=\"module-name\"]");
    private By fileInput = By.xpath("//*[@id=\"file-field\"]");
    private By urlInput = By.xpath("/html/body/div[1]/div[1]/div[1]/section/div[3]/div/div/section/form/div/div[2]/div[3]/input");

    public AddZip(WebDriver driver) {
        super(driver);
    }

    public AddZip setName(String name) {
        driver.findElement(nameInput).sendKeys(name);
        return this;
    }

    public AddZip setFile(File file) {
        driver.findElement(nameInput).sendKeys(file.getAbsolutePath());
        return this;
    }

    public AddZip setUrl(String url) {
        driver.findElement(nameInput).sendKeys(url);
        return this;
    }
}

