package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.DomainPages.AttacksPage;
import pages.DomainPages.DomainSettingPage;


public class SitesPage {
    WebDriver driver;
    private By firstSiteLink = By.xpath("/html/body/div/div[1]/div/section/div[3]/div[4]/section[1]/h2/a");

    public SitesPage(WebDriver driver) {
        this.driver = driver;
    }
    private By wafText = By.xpath("/html/body/div/div[1]/div/section/div[2]/div[4]/section[2]/div/div[2]/a/h3");
    private By createSiteButton = By.xpath("//a[@href=\"/domain/default/create\"]");

    public AttacksPage firstSiteLinkClick() {
        driver.findElement(firstSiteLink).click();
        return new AttacksPage(driver);
    }

    public boolean isWafGraphicsExist() {
        return driver.findElements(wafText).size() > 0;
    }

    public DomainSettingPage createSiteButtonClick() {
        driver.findElement(createSiteButton).click();
        return new DomainSettingPage(driver);
    }
}
