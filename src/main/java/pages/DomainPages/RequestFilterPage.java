package pages.DomainPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static java.lang.Thread.sleep;


public class RequestFilterPage {
    private WebDriver driver;
    private By requestTypeSelect = By.xpath("//*[@id=\"requestfilter-nginx_var\"]");
    private By regexInput = By.xpath("//input[@id=\"requestfilter-regexp\"]");
    private By addButton = By.xpath("//*[@id=\"w1\"]/div[3]/button");
    private By closeLabel = By.xpath("/html/body/div[1]/div[1]/div[1]/section/header/div/div[4]/section/section/label");
    private By popupBootboxText = By.xpath("//div[@class=\"bootbox-body\"]");
    private By okButton = By.xpath("/html/body/div[4]/div/div/div[2]/button");
    private By removeButton = By.xpath("//a[@ng-click=\"requestFilterPopup.remove(option.id)\"]");

    public RequestFilterPage(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isRequestTypeSelectExist() {
        return driver.findElements(requestTypeSelect).size() > 0;
    }

    public boolean isRegexInputExist() {
        return driver.findElements(regexInput).size() > 0;
    }

    public boolean isAddButtonExist() {
        return driver.findElements(addButton).size() > 0;
    }

    public List<WebElement> getRemoveButtonList() {
        return driver.findElements(removeButton);
    }

    private void typeRegexInput(String regex) throws InterruptedException {
        sleep(500);
        driver.findElement(regexInput).clear();
        driver.findElement(regexInput).sendKeys(regex);
    }

    private void addButtonClick() {
        driver.findElement(addButton).click();
    }

    private void okButtonClick() {
        driver.findElement(okButton).click();
    }

    private void selectRequestType(int index) {
        WebElement selectElem = driver.findElement(requestTypeSelect);
        Select select = new Select(selectElem);
        select.selectByIndex(index);
    }

    public void closeLabelClick() throws InterruptedException {
        sleep(500);
        driver.findElement(closeLabel).click();
        new AttacksPage(driver);
    }

    public void addNewFilter(int selectFilter, String regex) throws InterruptedException {
        this.selectRequestType(selectFilter);
        this.typeRegexInput(regex);
        this.addButtonClick();
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.presenceOfElementLocated(popupBootboxText));
        this.okButtonClick();
        // Wait until nginx restart
        sleep(10000);
    }

    public void removeFilter(int filterNumber) throws InterruptedException {
        List<WebElement> removeButtonList = this.getRemoveButtonList();
        sleep(1000);
        removeButtonList.get(filterNumber).click();
        driver.switchTo().alert().accept();
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.presenceOfElementLocated(popupBootboxText));
        this.okButtonClick();
        wait.until(ExpectedConditions.presenceOfElementLocated(popupBootboxText));
        this.okButtonClick();
    }
}
