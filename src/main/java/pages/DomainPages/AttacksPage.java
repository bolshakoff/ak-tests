package pages.DomainPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.CcsPage;

import static java.lang.Thread.sleep;

public class AttacksPage {
    private WebDriver driver;

    public AttacksPage(WebDriver driver) {
        this.driver = driver;
    }

    private By editLink = By.xpath("/html/body/div[1]/div[1]/div[1]/section/header/div/a");
    private By ccsLink = By.xpath("/html/body/div[1]/div[1]/div/section/div[2]/ul/li[3]/a");
    private By filterRequestLink = By.xpath("//button[@ng-click=\"requestFilterPopup.open()\"]");

    public DomainSettingPage editLinkClick() throws InterruptedException {
        driver.findElement(editLink).click();
        sleep(200);
        return new DomainSettingPage(driver);
    }

    public String getTitle() {
        return driver.getTitle();
    }

    public CcsPage ccsLinkClick() {
        driver.findElement(ccsLink).click();
        return new CcsPage(driver);
    }

    public boolean isFilterRequestLinkExist() {
        return driver.findElements(filterRequestLink).size() > 0;
    }

    public RequestFilterPage filterRequestLinkClick() throws InterruptedException {
        sleep(1000);
        driver.findElement(filterRequestLink).click();
        return new RequestFilterPage(driver);
    }

    public AttacksPage waitUntilSave() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.titleContains("Атаки"));
        sleep(500);
        return new AttacksPage(driver);
    }
}
