package pages.DomainPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.MainHeader;

import java.io.File;

import static java.lang.Thread.sleep;

public class DomainSettingPage {
    private WebDriver driver;
    private By domainNameInput = By.xpath("//input[@ng-model=\"domain.name\"]");
    private By domainPortInput = By.xpath("//input[@ng-model=\"domain.listen_port\"]");
    private By serverIpSelect = By.xpath("//select[@ng-model=\"domain.server_ip\"]");

    private By wafEnabledCheckbox = By.xpath("/html/body/div/div[1]/div/section/div[3]/div[4]/div[2]/h2/label/span/span");
    private By siteIpInput = By.xpath("/html/body/div/div[1]/div/section/div[3]/div[4]/div[2]/div[1]/div[1]/div/textarea");
    private By sendAccessLogsToSyslog = By.xpath("/html/body/div[1]/div[1]/div/section/div[3]/div[4]/div[2]/div[1]/div[3]/div/label/span");
    private By proxyToHttps = By.xpath("/html/body/div[1]/div[1]/div/section/div[3]/div[4]/div[2]/div[2]/div/label/span");
    private By wafModeSelect = By.xpath("//select[@ng-model=\"domain.waf_mode\"]");
    private By perimetrCheckbox = By.xpath("/html/body/div/div[1]/div/section/div[3]/div[4]/div[2]/div[5]/div/label/span");
    private By balancingModeSelect = By.xpath("//select[@ng-model=\"domain.balancing_type\"]");
    private By httpsEnableCheckbox = By.xpath("/html/body/div/div[1]/div/section/div[3]/div[4]/div[2]/div[8]/div/label/span");
    private By sslCertFile = By.xpath("//input[@file-model=\"domain.sslCertificate\"]");
    private By sslKeyFile = By.xpath("//input[@file-model=\"domain.sslCertificateKey\"]");
    private By httpsRedirectCheckbox = By.xpath("/html/body/div/div[1]/div/section/div[3]/div[4]/div[2]/div[10]/div[1]/div/label/span");
    private By redirectPortInput = By.xpath("//input[@ng-if=\"domain.ssl_redirect\"]");
    private By httpsProxyCheckbox = By.xpath("/html/body/div/div[1]/div/section/div[3]/div[4]/div[2]/div[10]/div[1]/div/label/span");
    private By analyzeServerResponseCheckbox = By.xpath("/html/body/div/div[1]/div/section/div[3]/div[4]/div[2]/div[15]/div/label/span");
    private By notAnalyzeStaticsCheckbox = By.xpath("/html/body/div/div[1]/div/section/div[3]/div[4]/div[2]/div[16]/div/label/span");
    private By page403File = By.xpath("//input[@file-model=\"domain.custom403\"]");
    private By saveButton = By.xpath("//a[@ng-click=\"save()\"]");
    private By removeButton = By.xpath("/html/body/div/div[1]/div/section/div/div[5]/a[3]");
    private By confirmRemoveButton = By.xpath("//button[@data-bb-handler=\"delete\"]");
    private By ccsEnableCheckbox = By.xpath("/html/body/div/div[1]/div/section/div[2]/div[4]/div[3]/h2/label/span/span");
    private By ccsEnableInfo = By.xpath("/html/body/div/div[1]/div/section/div/div[4]/div[3]/span[1]");

    public DomainSettingPage(WebDriver driver) {
        this.driver = driver;
    }

    public DomainSettingPage sendAccessLogsToSyslogClick() {
        driver.findElement(sendAccessLogsToSyslog).click();
        return this;
    }

    public DomainSettingPage proxyToHttpsClick() {
        driver.findElement(proxyToHttps).click();
        return this;
    }

    public DomainSettingPage perimetrCheckboxClick() {
        driver.findElement(perimetrCheckbox).click();
        return this;
    }

    public DomainSettingPage httpsRedirectCheckboxClick() {
        driver.findElement(httpsRedirectCheckbox).click();
        return this;
    }

    public DomainSettingPage httpsProxyCheckboxClick() {
        driver.findElement(httpsProxyCheckbox).click();
        return this;
    }

    public DomainSettingPage ccsEnableCheckboxClick() {
        driver.findElement(ccsEnableCheckbox).click();
        return this;
    }

    public DomainSettingPage analyzeServerResponseCheckboxClick() {
        driver.findElement(analyzeServerResponseCheckbox).click();
        return this;
    }

    public DomainSettingPage notAnalyzeStaticsCheckboxClick() {
        driver.findElement(notAnalyzeStaticsCheckbox).click();
        return this;
    }

    public String getCcsEnableInfo() {
        return driver.findElement(ccsEnableInfo).getText();
    }

    public DomainSettingPage typeDomainName(String siteDomainName) {
        driver.findElement(domainNameInput).clear();
        driver.findElement(domainNameInput).sendKeys(siteDomainName);
        return this;
    }

    public DomainSettingPage typeRedirectPort(String redirectPort) {
        driver.findElement(redirectPortInput).clear();
        driver.findElement(redirectPortInput).sendKeys(redirectPort);
        return this;
    }

    public DomainSettingPage typeDomainPort(String domainPort) {
        driver.findElement(domainPortInput).clear();
        driver.findElement(domainPortInput).sendKeys(domainPort);
        return this;
    }

    public DomainSettingPage selectServerIp(int index) throws InterruptedException {
        sleep(200);
        WebElement selectElem = driver.findElement(serverIpSelect);
        Select select = new Select(selectElem);
        select.selectByIndex(index);
        return this;
    }

    public DomainSettingPage selectWafMode(int index) {
        WebElement selectElem = driver.findElement(wafModeSelect);
        Select select = new Select(selectElem);
        select.selectByIndex(index);
        return this;
    }

    public DomainSettingPage selectBalancingMode(int index) {
        WebElement selectElem = driver.findElement(balancingModeSelect);
        Select select = new Select(selectElem);
        select.selectByIndex(index);
        return this;
    }

    public DomainSettingPage enabledCheckbox() {
        driver.findElement(wafEnabledCheckbox).click();
        return this;
    }

    public DomainSettingPage typeSiteIpInput(String siteIp) {
        driver.findElement(siteIpInput).sendKeys(siteIp);
        return this;
    }

    public DomainSettingPage enableHttps() {
        driver.findElement(httpsEnableCheckbox).click();
        return this;
    }

    public DomainSettingPage addSslCert(File file) {
        driver.findElement(sslCertFile).sendKeys(file.getAbsolutePath());
        return this;
    }

    public DomainSettingPage addSslKey(File file) {
        driver.findElement(sslKeyFile).sendKeys(file.getAbsolutePath());
        return this;
    }

    public DomainSettingPage add403Page(File file) {
        driver.findElement(page403File).sendKeys(file.getAbsolutePath());
        return this;
    }

    public AttacksPage saveButtonClick() {
        driver.findElement(saveButton).click();
        return new AttacksPage(driver);
    }

    public DomainSettingPage removeButtonClick() {
        driver.findElement(removeButton).click();
        return this;
    }

    public MainHeader confirmButtonClick() {
        driver.findElement(confirmRemoveButton).click();
        return new MainHeader(driver);
    }

    public MainHeader waitUntilRemove() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleIs("Сайты"));
        return new MainHeader(driver);
    }
}
