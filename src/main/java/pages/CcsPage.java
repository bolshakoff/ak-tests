package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CcsPage {
    private WebDriver driver;
    private By addModuleButton = By.xpath("/html/body/div[1]/div[1]/div[1]/section/div[3]/section[1]/header/div[2]/button");

    public CcsPage(WebDriver driver) {
        this.driver = driver;
    }

    public CcsPage addModuleButtonClick() {
        driver.findElement(addModuleButton).click();
        return this;
    }
}
